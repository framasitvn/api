from repository.baseRepository import BaseRepository
from db_info.db_connection import Connection

class CategoryRepository(BaseRepository):
    
    def __init__(self):        
        self.__connect = Connection(database= Connection.DataWarehouse)

    def get_all(self):
        query = "SELECT * FROM DIM_CATEGORY"        
        categories = self.__connect.get_data(query)
        return categories
        
    def get_by_id(self, id):
        query = "SELECT * FROM DIM_CATEGORY WHERE DC_CATEGORY_KEY = %d" %(id)
        category = self.__connect.get_data(query)
        return category
