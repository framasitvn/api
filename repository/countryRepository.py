from repository.baseRepository import BaseRepository
from db_info.db_connection import Connection

class CountryRepository(BaseRepository):
    
    def __init__(self):        
        self.__connect = Connection()

    def get_all(self):
        query = "SELECT CT_COUNTRY_ID, CT_CODE, CT_NAME FROM TBL_COUNTRY"        
        countries = self.__connect.get_data(query)
        return countries

    def get_by_id(self, id):
        query = "SELECT CT_COUNTRY_ID, CT_CODE, CT_NAME FROM TBL_COUNTRY WHERE CT_COUNTRY_ID = %d" %(id)
        country = self.__connect.get_data(query)
        return country

