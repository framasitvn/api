from repository.baseRepository import BaseRepository, StoredProcedureEnum
from entity.contactInfoEntity import ContactInfoMapper
from db_info.db_connection import Connection

class ContactInfoRepository(BaseRepository):

    def __init__(self):
        self.__connect = Connection()

    def get_all(self):
        query = "SELECT * FROM TBL_CONTACT_INFO"        
        output = self.__connect.get_data(query)
        data = ContactInfoMapper(output).mapper()
        return data
    
    def get_by_id(self, id):
        query = "SELECT * FROM TBL_CONTACT_INFO WHERE CTI_CONTACT_ID = %d" % (id)
        output = self.__connect.get_data(query)
        data = ContactInfoMapper(output).mapper()
        return data

    def insert(self, entity):
        try:    
            id = self.__connect.create_output_parameter(0)
            parameters = (id, 
                            entity.FactoryId, 
                            entity.Title, 
                            entity.FirstName,
                            entity.LastName,
                            entity.Email,
                            entity.PhoneNo,
                            entity.PhoneNo2,
                            entity.IsActive,  
                            entity.Username)
            output = self.__connect.execute_stored_procedure(StoredProcedureEnum.CONTACT_INFO_ADD_OR_UPDATE, parameters)
            self.__connect.commit()
            return output
        except Exception as ex:
            self.__connect.rollback()
            return ex
            
    def update(self, entity):
        try:
            parameters = (entity.Id, 
                            entity.FactoryId, 
                            entity.Title, 
                            entity.FirstName,
                            entity.LastName,
                            entity.Email,
                            entity.PhoneNo,
                            entity.PhoneNo2,
                            entity.IsActive,  
                            entity.Username)
            out = self.__connect.execute_stored_procedure(StoredProcedureEnum.CONTACT_INFO_ADD_OR_UPDATE, parameters)
            self.__connect.commit()
            return out
        except Exception as ex:
            return ex

    def delete(self, entity):
        try:
            parameters = (entity.Id, entity.Username)
            out = self.__connect.execute_stored_procedure(StoredProcedureEnum.CONTACT_INFO_DEACTIVATED, parameters)
            self.__connect.commit()
            return out
        except Exception as ex:
            return ex
            