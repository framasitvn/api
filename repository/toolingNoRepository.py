from repository.baseRepository import BaseRepository
from db_info.db_connection import Connection

class ToolingNoRepository(BaseRepository):
    
    def __init__(self):        
        self.__connect = Connection(database=Connection.DataWarehouse)

    def get_all(self):
        query = "SELECT * FROM DIM_TOOLING_NUMBER"        
        tooling_numbers = self.__connect.get_data(query)
        return tooling_numbers

    def get_by_id(self, id):
        query = "SELECT * FROM DIM_TOOLING_NUMBER WHERE DTN_TOOLING_KEY = %d" %(id)
        tooling_no = self.__connect.get_data(query)
        return tooling_no
