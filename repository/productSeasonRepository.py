from repository.baseRepository import BaseRepository
from entity.productSeasonEntity import ProductSeasonMapper
from db_info.db_connection import Connection

class ProductSeasonRepository(BaseRepository):

    def __init__(self):
        self.__connect = Connection()

    def get_report(self, factory, customer, season, productGroup, category):
        ps_procedure_name = "PROC_GET_PRODUCT_SEASON"
        ps_parameters = (factory, customer, season, productGroup, category)
        output = self.__connect.execute_stored_procedure(ps_procedure_name, ps_parameters, self.__connect.Connect)
        data = ProductSeasonMapper(output).mapper()                        
        return data
    
    def get_product_season_by_id(self, id: int):
        query = "SELECT * FROM VW_PRODUCT_SEASON WHERE PS_PROD_SEASON_ID = %d" % (id)
        data = self.__connect.get_data(query)
        product_season = ProductSeasonMapper(data).mapper()
        return product_season

    def get_product_season_by_factory_id(self, factory_id: int):
        query = "SELECT * FROM VW_PRODUCT_SEASON WHERE PS_FACTORY_ID = %d" % (factory_id)
        data = self.__connect.get_data(query)
        product_season_list = ProductSeasonMapper(data).mapper()
        return product_season_list

    def insert(self, product_season):
        """
        Add product season
        product_season: product season
        connect: connection to database
        """        
        ps_procedure_name = "PROC_ADD_OR_UPDATE_PRODUCT_SEASON"
        
        productSeason = self.__connect.create_output_parameter(0)
        ps_parameters = (productSeason,
                        product_season.FactoryId,
                        product_season.ProjectId,
                        product_season.CountryId,
                        # product_season.CustomerId,
                        # product_season.ProductGroupId,
                        # product_season.ToolingId,
                        # product_season.CategoryId,
                        # product_season.SizeId,
                        product_season.SeasonStart,
                        product_season.SeasonFor,
                        product_season.ShoeFactoryId,
                        product_season.LeadTimes,
                        product_season.IsDecoration,
                        product_season.SeasonalForecast,
                        product_season.MaxMonthPpic,
                        # product_season.MaxMonthForecast,
                        # product_season.MaxWeekForecast,
                        # product_season.MaxWeekPpic,
                        product_season.ColoursAmount,
                        product_season.SizesAmount,
                        product_season.MoldAmount,
                        product_season.PartWeightRunner,
                        product_season.CbdTotalShotWeight,
                        product_season.TotalShotWeight,
                        product_season.StepsAmount,
                        product_season.CleatMaterial,
                        product_season.Username)
                        
        output =  self.__connect.execute_stored_procedure(ps_procedure_name, ps_parameters)        
        product_season.Id = int(output[0])        
        return product_season
    
    def update(self, product_season):
        ps_procedure_name = "PROC_ADD_OR_UPDATE_PRODUCT_SEASON"
        ps_parameters = (product_season.Id,
                        product_season.FactoryId,
                        product_season.ProjectId,
                        product_season.CountryId,
                        # product_season.CustomerId,
                        # product_season.ProductGroupId,
                        # product_season.ToolingId,
                        # product_season.CategoryId,
                        # product_season.SizeId,
                        product_season.SeasonStart,
                        product_season.SeasonFor,
                        product_season.ShoeFactoryId,
                        product_season.LeadTimes,
                        product_season.IsDecoration,
                        product_season.SeasonalForecast,
                        product_season.MaxMonthPpic,
                        # product_season.MaxMonthForecast,
                        # product_season.MaxWeekForecast,
                        # product_season.MaxWeekPpic,
                        product_season.ColoursAmount,
                        product_season.SizesAmount,
                        product_season.MoldAmount,
                        product_season.PartWeightRunner,
                        product_season.CbdTotalShotWeight,
                        product_season.TotalShotWeight,
                        product_season.StepsAmount,
                        product_season.CleatMaterial,
                        product_season.Username)
        return self.__connect.execute_stored_procedure(ps_procedure_name, ps_parameters)   

    def delete(self, id:int):
        ps_procedure_name = "PROC_DELETE_PRODUCT_SEASON"
        result = self.__connect.execute_stored_procedure(ps_procedure_name, (id,))   
        return result
       

