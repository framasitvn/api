from repository.baseRepository import BaseRepository
from db_info.db_connection import Connection

class ShoeFactoryRepository(BaseRepository):
    
    def __init__(self):        
        self.__connect = Connection(database=Connection.DataWarehouse)

    def get_all(self):
        query = "SELECT * FROM DIM_SHOE_FACTORY"        
        shoe_factories = self.__connect.get_data(query)
        return shoe_factories

    def get_by_id(self, id):
        query = "SELECT * FROM DIM_SHOE_FACTORY WHERE DSF_SHOE_FAC_KEY = %d" %(id)
        shoe_factory = self.__connect.get_data(query)
        return shoe_factory
