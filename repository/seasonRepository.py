from repository.baseRepository import BaseRepository, StoredProcedureEnum
from entity.seasonEntity import SeasonMapper
from db_info.db_connection import Connection

class SeasonRepository(BaseRepository):
    
    def __init__(self):
        self._connect = Connection()
    
    def get_all(self):
        query = "SELECT * FROM TBL_SEASON"        
        output = self.__connect.get_data(query)
        data = SeasonMapper(output).mapper()
        return data
         
    def get_by_id(self, id):
        query = "SELECT * FROM TBL_SEASON WHERE SS_SEASON_ID = %d" % (id)
        output = self.__connect.get_data(query)
        data = SeasonMapper(output).mapper()
        return data

    def insert(self, entity):
        try:    
            procedure_name = StoredProcedureEnum.SEASON_ADD_OR_UPDATE
            id = self.__connect.create_output_parameter(0)
            parameters = (id, 
                            entity.Code, 
                            entity.Name, 
                            entity.StartDate,
                            entity.EndDate,
                            entity.IsActive,  
                            entity.Username)
            output = self.__connect.execute_stored_procedure(procedure_name, parameters)
            return output
        except Exception as ex:
            return ex
            
    def update(self, entity):
        try:
            procedure_name = StoredProcedureEnum.SEASON_ADD_OR_UPDATE
            parameters = (entity.Id, 
                            entity.Code, 
                            entity.Name, 
                            entity.StartDate,
                            entity.EndDate,
                            entity.IsActive,  
                            entity.Username)
            return self.__connect.execute_stored_procedure(procedure_name, parameters)
        except Exception as ex:
            return ex

    def delete(self, entity):
        try:
            procedure_name = StoredProcedureEnum.SEASON_DEACTIVATED
            parameters = (entity.Id, entity.Username)
            return self.__connect.execute_stored_procedure(procedure_name, parameters)
        except Exception as ex:
            return ex
            