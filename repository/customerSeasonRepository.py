from repository.baseRepository import BaseRepository
from db_info.db_connection import Connection

class CustomerSeasonRepository(BaseRepository):

    def __init__(self):
        self.__connect = Connection()

    def get_all(self):
        query = "SELECT * FROM TBL_CUST_SEASON"        
        data = self.__connect.get_data(query)
        return data
    
    
