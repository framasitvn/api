from repository.baseRepository import BaseRepository
from db_info.db_connection import Connection

class ProjectRepository(BaseRepository):
    
    def __init__(self):        
        self.__connect = Connection(database= Connection.DataWarehouse)

    def get_all(self):
        query = """SELECT 
                        DP_PROJECT_KEY, 
                        DP_PROJECT_ID, DP_PROJECT_NO, DP_PROJECT_CODE, DP_PROJECT_NAME, DP_PROJECT_MOLDS, DP_DF_FACTORY_KEY, 
	                    DP_PRJ_DEV_DATE, DP_PRJ_MOLD_MAKING_DATE, DP_PRJ_MASS_PROD_DATE, DP_PRJ_DESCRIPTION, DP_PRJ_REMARKS 
                    FROM DIM_PROJECT"""
        projects = self.__connect.get_data(query)
        return projects

    def get_by_factory_id(self, factory_id:int):
        query = """SELECT 
                        DP_PROJECT_KEY, 
                        DP_PROJECT_ID, DP_PROJECT_NO, DP_PROJECT_CODE, DP_PROJECT_NAME, DP_PROJECT_MOLDS, DP_DF_FACTORY_KEY, 
	                    DP_PRJ_DEV_DATE, DP_PRJ_MOLD_MAKING_DATE, DP_PRJ_MASS_PROD_DATE, DP_PRJ_DESCRIPTION, DP_PRJ_REMARKS 
                    FROM DIM_PROJECT 
                    WHERE DP_DF_FACTORY_KEY = %d""" %(factory_id)
        projects = self.__connect.get_data(query)
        return projects

    def get_by_id(self, id: int):
        query = """SELECT 
                        DP_PROJECT_KEY, 
                        DP_PROJECT_ID, DP_PROJECT_NO, DP_PROJECT_CODE, DP_PROJECT_NAME, DP_PROJECT_MOLDS, DP_DF_FACTORY_KEY, 
	                    DP_PRJ_DEV_DATE, DP_PRJ_MOLD_MAKING_DATE, DP_PRJ_MASS_PROD_DATE, DP_PRJ_DESCRIPTION, DP_PRJ_REMARKS 
                    FROM DIM_PROJECT 
                    WHERE DP_PROJECT_KEY = %d""" %(id)
        project = self.__connect.get_data(query)
        return project
