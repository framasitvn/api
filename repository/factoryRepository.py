from repository.baseRepository import BaseRepository
from db_info.db_connection import  Connection

class FactoryRepository(BaseRepository):
    
    def __init__(self):        
        self.__connect = Connection(database= Connection.DataWarehouse)        


    def get_all(self):
        query = "SELECT * FROM DIM_FACTORY"        
        factories = self.__connect.get_data(query)
        return factories
        
    def get_active_factory(self):
        query = "SELECT * FROM DIM_FACTORY WHERE DF_FAC_IS_ACTIVE = 'TRUE'"        
        factories = self.__connect.get_data(query)
        return factories

    def get_by_id(self, id):
        query = "SELECT * FROM DIM_FACTORY WHERE DF_FACTORY_KEY = %d" %(id)
        factory = self.__connect.get_data(query)
        return factory
