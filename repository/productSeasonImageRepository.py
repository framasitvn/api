from repository.baseRepository import BaseRepository
from entity.productSeasonImageEntity import ProductSeasonImageMapper
from db_info.db_connection import Connection

class ProductSeasonImageRepository(BaseRepository):
    
    def __init__(self):
        self.__connect = Connection()

    def get_ps_images_by_ps_id(self, product_season_id: int):
        query = "SELECT * FROM TBL_IMAGES WHERE IMG_PS_PROD_SEASON_ID = %d" % (product_season_id)
        output = self.__connect.get_data(query)
        data = ProductSeasonImageMapper(output).mapper()
        return data

    def get_ps_image_by_id(self, id: int):
        query = "SELECT * FROM TBL_IMAGES WHERE IMG_ID = %d" % (id)
        output = self.__connect.get_data(query)
        data = ProductSeasonImageMapper(output).mapper()
        return data

    def insert(self, image):
        img_procedure_name = "PROC_ADD_OR_UPDATE_PS_IMAGES"                                
        img_parameters = (None,
                        image.ProductSeasonId, 
                        image.DocumentId, 
                        image.FileName, 
                        image.FileExtend, 
                        image.Path, 
                        image.Username)
        return self.__connect.execute_stored_procedure(img_procedure_name, img_parameters)
    
    def update(self, image):
        img_procedure_name = "PROC_ADD_OR_UPDATE_PS_IMAGES"                                
        img_parameters = (image.Id,
                        image.ProductSeasonId, 
                        image.DocumentId, 
                        image.FileName, 
                        image.FileExtend, 
                        image.Path, 
                        image.Username)
        return self.__connect.execute_stored_procedure(img_procedure_name, img_parameters)
    

    def delete(self, id:int):
        try:            
            ps_procedure_name = "PROC_DELETE_PS_IMAGES"
            self.__connect.execute_stored_procedure(ps_procedure_name, id)
            self.__connect.commit()
        except Exception as ex:
            self.__connect.rollback()
            return ex
