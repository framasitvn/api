from repository.baseRepository import BaseRepository
from entity.productSeasonComponentEntity import ProductSeasonComponentMapper
from db_info.db_connection import Connection

class ProductSeasonComponentRepository(BaseRepository):

    def __init__(self):
        self.__connect = Connection()

    def get_ps_component_by_ps_id(self, product_season_id: int):
        query = "SELECT * FROM TBL_PS_COMPONENT WHERE PSC_PS_PROD_SEASON_ID = %d" % (product_season_id)
        output = self.__connect.get_data(query)
        data = ProductSeasonComponentMapper(output).mapper()
        return data

    def get_ps_component_by_id(self, id: int):
        query = "SELECT * FROM TBL_PS_COMPONENT WHERE PSC_COMPONENT_ID = %d" % (id)
        output = self.__connect.get_data(query)
        data = ProductSeasonComponentMapper(output).mapper()
        return data

    def insert(self, component):        
        cp_procedure_name = "PROC_ADD_OR_UPDATE_PS_COMPONENT"                                
        cp_parameters = (None,
                        component.ComponentId, 
                        component.ProductSeasonId, 
                        component.Index, 
                        component.ShotWeight, 
                        component.MainMaterial, 
                        component.AltMatTrans, 
                        component.AltMatBlack,
                        component.Username)
        return self.__connect.execute_stored_procedure(cp_procedure_name, cp_parameters)
        

    def update(self, component):
        cp_procedure_name = "PROC_ADD_OR_UPDATE_PS_COMPONENT"                                
        cp_parameters = (component.Id,
                        component.ComponentId, 
                        component.ProductSeasonId, 
                        component.Index, 
                        component.ShotWeight, 
                        component.MainMaterial, 
                        component.AltMatTrans, 
                        component.AltMatBlack,
                        component.Username)
        return self.__connect.execute_stored_procedure(cp_procedure_name, cp_parameters)
    

    def delete(self, id:int):
        try:            
            ps_procedure_name = "PROC_DELETE_PS_COMPONENT"
            self.__connect.execute_stored_procedure(ps_procedure_name, id)   
            self.__connect.commit()
        except Exception as ex:
            self.__connect.rollback()
            return ex
