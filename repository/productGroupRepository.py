from repository.baseRepository import BaseRepository
from db_info.db_connection import Connection

class ProductGroupRepository(BaseRepository):
    
    def __init__(self, connection):        
        self.__connect = Connection(database=Connection.DataWarehouse)

    def get_all(self):
        query = "SELECT * FROM DIM_PRODUCT_GROUP"        
        product_groups = self.__connect.get_data(query)
        return product_groups
        
    def get_by_id(self, id):
        query = "SELECT * FROM DIM_PRODUCT_GROUP WHERE DPG_PROD_GROUP_KEY = %d" %(id)
        product_group = self.__connect.get_data(query)
        return product_group
