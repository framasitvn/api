from repository.baseRepository import BaseRepository
from entity.productSeasonPriceEntity import ProductSeasonPriceMapper
from db_info.db_connection import Connection

class ProductSeasonPriceRepository(BaseRepository):

    def __init__(self):
        self.__connect = Connection()

    def get_ps_price_by_ps_id(self, product_season_id: int):
        query = "SELECT * FROM TBL_PS_PRICE WHERE PSP_PS_PROD_SEASON_ID = %d" % (product_season_id)
        output = self.__connect.get_data(query)
        data = ProductSeasonPriceMapper(output).mapper()
        return data

    def get_ps_price_by_id(self, id: int):
        query = "SELECT * FROM TBL_PS_PRICE WHERE PSP_PRICE_ID = %d" % (id)
        output = self.__connect.get_data(query)
        data = ProductSeasonPriceMapper(output).mapper()
        return data
 
    def insert(self, price):
        price_procedure_name = "PROC_ADD_OR_UPDATE_PS_PRICE"                                
        price_parameters = (None, 
                            price.ProductSeasonId, 
                            price.Index, 
                            price.Value, 
                            price.Description, 
                            price.IsMainPrice, 
                            price.Username)
        return self.__connect.execute_stored_procedure(price_procedure_name, price_parameters)
    

    def update(self, price):
        price_procedure_name = "PROC_ADD_OR_UPDATE_PS_PRICE"                                
        price_parameters = (price.Id, 
                            price.ProductSeasonId, 
                            price.Index, 
                            price.Value, 
                            price.Description, 
                            price.IsMainPrice, 
                            price.Username)
        return self.__connect.execute_stored_procedure(price_procedure_name, price_parameters)
    

    def delete(self, id:int):
        try:            
            ps_procedure_name = "PROC_DELETE_PS_PRICE"
            result = self.__connect.execute_stored_procedure(ps_procedure_name, id)   
            self.__connect.commit()
            return result
        except Exception as ex:
            self.__connect.rollback()
            return ex