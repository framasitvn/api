from repository.baseRepository import BaseRepository
from models.customerCatalogModel import CustomerCatalogMapper
from db_info.db_connection import Connection

class ReportRepository(BaseRepository):
    
    def __init__(self):        
        self.Connect = Connection()


    def get_customer_category(self, param):
        procedure_name = "PROC_GET_CUSTOMER_CATALOG_REPORT"        
        parameters = (param.FactoryId,
                      param.CustomerId,
                      param.SeasonCode,
                      param.ProductGroupId,
                      param.CategoryId)
        data = self.Connect.get_data_by_stored_procedure(procedure_name, parameters)
        report = CustomerCatalogMapper(data).mapper()
        return data if isinstance(report, Exception) else report
            

        