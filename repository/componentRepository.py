from repository.baseRepository import BaseRepository, StoredProcedureEnum
from entity.componentEntity import ComponentMapper
from db_info.db_connection import Connection

class ComponentRepository(BaseRepository):
    
    def __init__(self):
        self.__connect = Connection()

    def get_all(self):
        query = "SELECT * FROM TBL_COMPONENT"        
        output = self.__connect.get_data(query)
        data = ComponentMapper(output).mapper()
        return data
    
    def get_by_id(self, id):
        query = "SELECT * FROM TBL_COMPONENT WHERE CP_COMP_ID = %d" % (id)
        output = self.__connect.get_data(query)
        data = ComponentMapper(output).mapper()
        return data

    def insert(self, entity):#, connect):                                
        try:    
            procedure_name = StoredProcedureEnum.COMPONENT_ADD_OR_UPDATE
            cp_id = self.__connect.create_output_parameter(0)
            cp_parameters = (cp_id, 
                            entity.Code, 
                            entity.Name, 
                            entity.IsActive,  
                            entity.Username)
            output = self.__connect.execute_stored_procedure(procedure_name, cp_parameters)#, connect)
            return output
        except Exception as ex:
            return ex
            
    def update(self, entity):
        try:
            procedure_name = StoredProcedureEnum.COMPONENT_ADD_OR_UPDATE
            cp_parameters = (entity.Id, 
                            entity.Code, 
                            entity.Name, 
                            entity.IsActive,  
                            entity.Username)
            return self.__connect.execute_stored_procedure(procedure_name, cp_parameters)
        except Exception as ex:
            return ex

    def delete(self, entity):
        try:
            procedure_name = StoredProcedureEnum.COMPONENT_DEACTIVATED
            cp_parameters = (entity.Id, entity.Username)
            return self.__connect.execute_stored_procedure(procedure_name, cp_parameters)
        except Exception as ex:
            return ex
            