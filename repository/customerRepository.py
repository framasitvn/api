from repository.baseRepository import BaseRepository
from db_info.db_connection import Connection

class CustomerRepository(BaseRepository):
    
    def __init__(self):        
        self.__connect = Connection(database= Connection.DataWarehouse)

    def get_all(self):
        query = "SELECT * FROM DIM_CUSTOMER"        
        customers = self.__connect.get_data(query)
        return customers
        
    def get_by_id(self, id):
        query = "SELECT * FROM DIM_CUSTOMER WHERE DC_CUST_KEY = %d" %(id)
        customer = self.__connect.get_data(query)
        return customer
