from repository.baseRepository import BaseRepository
from entity.productSeasonShoeFactoryEntity import ProductSeasonShoeFactoryMapper
from db_info.db_connection import Connection

class ProductSeasonShoeFactoryRepository(BaseRepository):
    
    def __init__(self):
        self.__connect = Connection()

    def get_ps_shoes_factory_by_ps_id(self, product_season_id: int):
        query = "SELECT * FROM TBL_PS_SHOE_FACTORY WHERE PSSF_PS_PROD_SEASON_ID = %d" % (product_season_id)
        output = self.__connect.get_data(query)
        data = ProductSeasonShoeFactoryMapper(output).mapper()
        return data

    # def get_ps_image_by_id(self, id: int):
    #     query = "SELECT * FROM TBL_IMAGES WHERE IMG_ID = %d" % (id)
    #     output = self.get_data(query)
    #     data = ProductSeasonShoeFactoryMapper(output).mapper()
    #     return data

    def insert(self, shoe_factory):
        sf_procedure_name = "PROC_ADD_OR_UPDATE_PS_SHOE_FACTORY"                                
        sf_parameters = (None,
                        shoe_factory.ProductSeasonId, 
                        shoe_factory.ShoeFactoryId,                             
                        shoe_factory.Username)
        return self.__connect.execute_stored_procedure(sf_procedure_name, sf_parameters)
    
    def change_shoe_factory(self, shoe_factory):
        sf_procedure_name = "PROC_ADD_OR_UPDATE_PS_SHOE_FACTORY"                                
        sf_parameters = (shoe_factory.Id,
                        shoe_factory.ProductSeasonId, 
                        shoe_factory.ShoeFactoryId,                             
                        shoe_factory.Username)
        return self.__connect.execute_stored_procedure(sf_procedure_name, sf_parameters)

    