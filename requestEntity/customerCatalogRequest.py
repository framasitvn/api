from  app_response import RequestError
from requestEntity.baseRequest import BaseRequest

class CustomerCatalogRequest():

    def __init__(self, request):
        base = BaseRequest(request)
        self.FactoryId = base.get_field_value("factory_id", 0)
        self.CustomerId = base.get_field_value("customer_id", 0)
        self.SeasonCode = base.get_field_value("season_code", '')
        self.ProductGroupId = base.get_field_value("product_group_id", 0)
        self.CategoryId = base.get_field_value("category_id", 0)

    
