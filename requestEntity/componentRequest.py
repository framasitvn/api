from  app_response import RequestError
from requestEntity.baseRequest import BaseRequest, Action

class ComponentRequest(BaseRequest):

    def __init__(self, request, id:int= None, action:Action = None):
        super().__init__(request)
        if action not in [Action.DELETE, Action.DELETE]:
            self.__validated()
        self.Id = self.get_field_value("id", id)
        self.Code = self.get_field_value('code')
        self.Name = self.get_field_value('name')
        self.IsActive = self.get_field_value("is_active", True)

    def __validated(self):
        self.required_request_error("code")
        self.required_request_error("name")
