from  app_response import RequestError
from requestEntity.baseRequest import BaseRequest

class ProductSeasonShoeFactoryRequest(BaseRequest):
    
    def __init__(self, request, id: int = None, product_season_id: int = None, username: str = None):
        """
        request: json request
        when id == 0:
            id:  required
            product_season_id: required 
            username: required
        """
        super().__init__(request, username)
        self.required_request_error("shoe_factory_id")
        
      
        self.Id = self.get_field_value('id', id)
        self.ProductSeasonId = self.get_field_value('product_season_id', product_season_id)
        self.ShoeFactoryId = self.get_field_value("shoe_factory_id")
        
