from  app_response import RequestError
from requestEntity.baseRequest import BaseRequest

class ProductSeasonComponentRequest(BaseRequest):
    
    def __init__(self, request, id: int = None, product_season_id: int = None, username: str = None):
        """
        request: json request
        status: if not status then modify the price
        when not status:
            id:  required
            product_season_id: required 
            username: required
        """
        super().__init__(request, username)
        self.required_request_error("index")
        self.required_request_error("component_id")
        self.required_request_error("shot_weight")

        self.Id = self.get_field_value('id', id)
        self.ProductSeasonId = self.get_field_value('product_season_id', product_season_id)
        self.ComponentId = self.get_field_value("component_id")
        self.Index = self.get_field_value("index")
        self.ShotWeight = self.get_field_value("shot_weight")
        self.MainMaterial = self.get_field_value("main_material")
        self.AltMatTrans = self.get_field_value("alt_mat_trans")
        self.AltMatBlack = self.get_field_value("alt_mat_black")

     