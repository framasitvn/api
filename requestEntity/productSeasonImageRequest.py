from  app_response import RequestError
from requestEntity.baseRequest import BaseRequest

class ProductSeasonImageRequest(BaseRequest):
    
    def __init__(self, request, id: int = None, product_season_id: int = None, username: str = None):
        """
        request: json request
        status: if not status then modify the image
        when not status:
            id:  required
            product_season_id: required 
            username: required
        """
        super().__init__(request, username)
        self.required_request_error("file_name")
        self.required_request_error("file_extend")
        
        self.Id = self.get_field_value('id', id)
        self.ProductSeasonId = self.get_field_value('product_season_id', product_season_id)
        self.DocumentId = self.get_field_value("document_id")
        self.FileName = self.get_field_value("file_name")
        self.FileExtend = self.get_field_value("file_extend")
        self.Path = self.get_field_value("path")

     