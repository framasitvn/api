import json
from app_response import RequestError
from enum import Enum

class BaseRequest():
    # Username : str
    # Username = "admin@custcatalogue.framas.com"

    def __init__(self, request, username: str = None):
        self.request = self.__lower_keys(request)
    #     if not username:
    #         self.required_request_error("username")
    #         self.Username = self.request['username']
    #     else:
    #         self.Username =  username

    def required_request_error(self, field:str):
        if field not in self.request:
            raise RequestError("%s is required" % field)

    def get_field_value(self, field:str, default= None):
        if field in self.request:
            return self.request[field]
        else:
            if default is not None:
                if default == 0 or default == '':
                    self.required_request_error(field)
            return default

    def __lower_keys(self, data):
        if isinstance(data, list):
            return [self.__lower_keys(value) for value in data]
        elif isinstance(data, dict):
            return dict((key.lower(), self.__lower_keys(value)) for key, value in data.items())
        else:
            return data
            
class ApiRequest():

    def __init__(self, meta_request):
        if "username" not in meta_request:
            raise RequestError("username is required!")
        elif "meta_data" not in meta_request:
            raise RequestError("meta_data is required!")
        else:
            self.Data = meta_request["meta_data"]
            self.Username = meta_request["username"]

class Action(Enum):
    ADD = 1,
    UPDATE = 2,
    DELETE = 3,
    DEACTIVE = 4
    


