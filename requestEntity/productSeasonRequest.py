from  app_response import RequestError
from requestEntity.baseRequest import BaseRequest
   

class ProductSeasonRequest(BaseRequest):
    
    def __init__(self, request, id: int = None):
        super().__init__(request)
        self.__validated()
        self.Id = self.get_field_value("id", id)
        self.FactoryId = self.get_field_value('factory_id')
        self.ProjectId = self.get_field_value('project_id')
        self.CountryId = self.get_field_value('country_id')
        # self.CustomerId = self.get_field_value('customer_id')
        # self.ProductGroupId = self.get_field_value('product_group_id')
        # self.CategoryId = self.get_field_value('category_id')
        # self.ToolingId = self.get_field_value('tooling_id')
        # self.SizeId = self.get_field_value('size_id')
        self.SeasonFor = self.get_field_value('season_for')
        self.SeasonStart = self.get_field_value('season_start')
        self.ShoeFactoryId = self.get_field_value('shoe_factory_id')
        self.LeadTimes = self.get_field_value('lead_times')
        self.IsDecoration = self.get_field_value('is_decoration')
        self.SeasonalForecast = self.get_field_value('seasonal_forecast')
        self.MaxMonthPpic = self.get_field_value('max_month_ppic')
        # self.MaxMonthForecast = self.get_field_value('max_month_forecast')
        # self.MaxWeekForecast = self.get_field_value('max_week_forecast')
        # self.MaxWeekPpic = self.get_field_value('max_week_ppic')
        
        self.ColoursAmount = self.get_field_value('colours_amount')
        self.SizesAmount = self.get_field_value('sizes_amount')
        self.MoldAmount = self.get_field_value('mold_amount')
        self.PartWeightRunner = self.get_field_value('part_weight_runner')
        self.CbdTotalShotWeight = self.get_field_value('cbd_total_shot_weight')
        
        # self.TotalShotWeight = self.get_field_value('total_shot_weight')
        # self.StepsAmount = self.get_field_value('steps_amount')
        
        self.CleatMaterial = self.get_field_value('cleat_material')
        #list objects input 
        self.Prices = self.get_field_value('prices')
        if len(self.Prices) == 0:
            raise RequestError('Data must have at least one price')
        self.Images = self.get_field_value('images')
        self.Components = self.get_field_value('components')
        self.StepsAmount = 0 if self.Components == None else len(self.Components)
        self.TotalShotWeight = 0 
        self.ShoeFactories = self.get_field_value('shoe_factories')
        

    def __validated(self):
        self.required_request_error("factory_id")
        self.required_request_error("project_id")
        self.required_request_error("season_for")
        # self.required_request_error("customer_id")
        # self.required_request_error("product_group_id")
        # self.required_request_error("category_id")
        self.required_request_error("prices")

