from  app_response import RequestError
from requestEntity.baseRequest import BaseRequest, Action

class SeasonRequest(BaseRequest):

    def __init__(self, request, id:int= None, action:Action= None):
        super().__init__(request)
        if action not in [Action.DEACTIVE, Action.DELETE]:
            self.__validated()
        self.Id = self.get_field_value("id", id)
        self.Code = self.get_field_value('code')
        self.Name = self.get_field_value('name')
        self.StartDate = self.get_field_value('start_date')
        self.EndDate = self.get_field_value('end_date')
        self.IsActive = self.get_field_value("is_active", True)

    def __validated(self):
        self.required_request_error("code")
        self.required_request_error("name")
        self.required_request_error("start_date")
        self.required_request_error("end_date")