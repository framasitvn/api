from  app_response import RequestError
from requestEntity.baseRequest import BaseRequest, Action

class ContactInfoRequest(BaseRequest):

    def __init__(self, request, id:int= None, action:Action = None):
        super().__init__(request)
        if action not in [Action.DEACTIVE, Action.DELETE]:
            self.__validated()
        self.Id = self.get_field_value("id", id)
        self.FactoryId = self.get_field_value('factory_id')
        self.Title = self.get_field_value('title')
        self.FirstName = self.get_field_value('first_name')
        self.LastName = self.get_field_value('last_name')
        self.Email = self.get_field_value('email')
        self.PhoneNo = self.get_field_value('phone_no')
        self.PhoneNo2 = self.get_field_value('phone_no_2')        
        self.IsActive = self.get_field_value("is_active", True)

    def __validated(self):
        self.required_request_error("factory_id")
        self.required_request_error("title")
        self.required_request_error("first_name")
        self.required_request_error("last_name")
        self.required_request_error("email")
