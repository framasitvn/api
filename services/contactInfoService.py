from repository.contactInfoRepository import ContactInfoRepository

class ContactInfoService():
    
    def __init__(self):
        """
        """        
        self.contactInfoRepository = ContactInfoRepository()
        
    
    def get_all(self):
        """
        """
        return self.contactInfoRepository.get_all()
        
    def get_by_id(self, id:int):        
        """
        """
        return self.contactInfoRepository.get_by_id(id)
        
    def add_contact_info(self, contact_info):   
        """
        """
        with self.conn.Connect as connect:
            contact_id = self.contactInfoRepository.insert(contact_info)
            connect.commit()
            data = self.get_by_id(contact_id[0])
            connect.close()
        return data

    
    def update_contact_info(self, contact_info):
        """
        """
        with self.conn.Connect as connect:
            contact_id = self.contactInfoRepository.update(contact_info)
            connect.commit()
            data = self.get_by_id(contact_id[0])
            connect.close()
        return data

    def delete_contact_info(self, contact_info):
        """
        """
        with self.conn.Connect as connect:
            data =  self.contactInfoRepository.delete(contact_info)
            connect.commit()
            connect.close()
        return not isinstance(data, Exception)
    
   