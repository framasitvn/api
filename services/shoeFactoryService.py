from repository.shoeFactoryRepository import ShoeFactoryRepository
from entity.shoeFactoryEntity  import ShoeFactoryMapper

class ShoeFactoryService():
    

    def __init__(self):
        """
        """        
        self.shoeFactoryRepository = ShoeFactoryRepository()

    def get_all(self):
        """
        """
        shoe_factories = self.shoeFactoryRepository.get_all()
        data = ShoeFactoryMapper(shoe_factories).mapper()                
        return data
        

    def get_by_id(self, id: int):
        """
        """
        shoe_factory = self.shoeFactoryRepository.get_by_id(id)
        data = ShoeFactoryMapper(shoe_factory).mapper()                
        return data
    
