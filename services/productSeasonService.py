from repository.productSeasonRepository import ProductSeasonRepository
from repository.productSeasonPriceRepository import ProductSeasonPriceRepository
from repository.productSeasonComponentRepository import ProductSeasonComponentRepository
from repository.productSeasonImageRepository import ProductSeasonImageRepository
from repository.productSeasonShoeFactoryRepository import ProductSeasonShoeFactoryRepository

#from app_response import ResponseError


class ProductSeasonService():
    
    def __init__(self):
        """
        """        
        self.productSeasonRepository = ProductSeasonRepository()
        self.productSeasonPriceRepository = ProductSeasonPriceRepository()
        self.productSeasonImageRepository = ProductSeasonImageRepository()
        self.productSeasonComponentRepository = ProductSeasonComponentRepository()
        self.productSeasonShoeFactoryRepository = ProductSeasonShoeFactoryRepository()

    # def get_all(self):
    #     components = self.componentRepository.get_all()
    #     data = ComponentMapper(components).mapper()                
    #     return data
    
    def get_product_season_by_id(self, id:int):
        """
        Return object Product season 
        """
        product_season = self.productSeasonRepository.get_product_season_by_id(id)
        if len(product_season) > 0: 
            product_season[0]["Prices"] = self.get_prices_by_product_season_id(id)
            product_season[0]["Components"] = self.get_components_by_product_season_id(id)
            product_season[0]["Images"] = self.get_images_by_product_season_id(id)
            product_season[0]["ShoeFactories"] = self.get_shoes_factory_by_product_season_id(id)
        else:
            product_season = None
        return product_season
    
    def get_product_season_by_factory_id(self, factory_id: int):
        """
        Return list product season
        """
        product_season_list = self.productSeasonRepository.get_product_season_by_factory_id(factory_id)
        return product_season_list

    def get_prices_by_product_season_id(self, product_season_id:int):
        """
        Return list prices
        """
        prices = self.productSeasonPriceRepository.get_ps_price_by_ps_id(product_season_id)        
        return self.__get_value(prices)
    
    def get_components_by_product_season_id(self, product_season_id:int):
        """
        Return list components
        """
        components = self.productSeasonComponentRepository.get_ps_component_by_ps_id(product_season_id)
        return self.__get_value(components)


    def get_images_by_product_season_id(self, product_season_id:int):
        """
        Return list images
        """
        images = self.productSeasonImageRepository.get_ps_images_by_ps_id(product_season_id)
        return self.__get_value(images)
        
    def get_shoes_factory_by_product_season_id(self, product_season_id:int):
        """
        Return list shoes factory
        """
        shoes_factory = self.productSeasonShoeFactoryRepository.get_ps_shoes_factory_by_ps_id(product_season_id)
        return self.__get_value(shoes_factory)
        
    def add(self, product_season, prices, components = None, images = None, shoe_factories = None):        
        """
        """
        try:
            with self.Connection.Connect as connect:
                #productSeason = ProductSeasonRequest(product_season)
                self.productSeasonRepository.insert(product_season)
                for price in prices:
                    price.ProductSeasonId = product_season.Id
                    price.Username = product_season.Username                
                    self.productSeasonPriceRepository.insert(price)
                
                #processing for saving all images of product season
                if images is not None:
                    for image in images:                    
                        image.ProductSeasonId = product_season.Id
                        image.Username = product_season.Username
                        self.productSeasonImageRepository.insert(image)
                
                #processing for saving components of product season
                if components is not None:
                    for component in components:                
                        component.ProductSeasonId = product_season.Id
                        component.Username = product_season.Username
                        self.productSeasonComponentRepository.insert(component)
                
                #processing for saving all shoe factories
                if shoe_factories is not None:
                    for shoe_factory in shoe_factories:                    
                        shoe_factory.ProductSeasonId = product_season.Id
                        shoe_factory.Username = product_season.Username
                        self.productSeasonShoeFactoryRepository.insert(shoe_factory)

                # self.Connection.commit()
                connect.commit()                
                data = self.productSeasonRepository.get_product_season_by_id(product_season.Id)
                connect.close()
            return data
        except Exception as ex:
            connect.rollback()
            raise Exception(ex.__dict__)
    
    def update(self, product_season, prices, components = None, images = None, shoe_factories = None):        
        """
        """
        try:
            with self.Connection.Connect as connect:
                self.productSeasonRepository.update(product_season)
                for price in prices:
                    price.ProductSeasonId = product_season.Id
                    price.Username = product_season.Username
                    self.productSeasonPriceRepository.update(price)

                #processing for saving all images of product season
                if images is not None:
                    for image in images:
                        image.ProductSeasonId = product_season.Id
                        image.Username = product_season.Username
                        self.productSeasonImageRepository.update(image)
                
                #processing for saving components of product season
                if components is not None:
                    for component in components:
                        component.ProductSeasonId = product_season.Id
                        component.Username = product_season.Username
                        self.productSeasonComponentRepository.update(component)
                
                #processing for saving all shoe factories
                if shoe_factories is not None:
                    for shoe_factory in shoe_factories:
                        shoe_factory.ProductSeasonId = product_season.Id
                        shoe_factory.Username = product_season.Username
                        self.productSeasonShoeFactoryRepository.change_shoe_factory(shoe_factory)
                        
                connect.commit()
                data = self.productSeasonRepository.get_product_season_by_id(product_season.Id)
                connect.close()
                return data
        except Exception as ex:
            connect.rollback()
            raise Exception(ex.__dict__)

        
    def delete(self, id:int = None):
        """
        """
        with self.Connection.Connect as connect:
            result = self.productSeasonRepository.delete(id)
            connect.commit()
            connect.close()
        return result

    def __get_value(self, data):
        return data if len(data) > 0 else None