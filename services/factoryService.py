from repository.factoryRepository import FactoryRepository
from entity.factoryEntity  import FactoryMapper


class FactoryService():
    

    def __init__(self):
        """
        """
        self.factoryRepository = FactoryRepository()

    def get_all(self):
        """
        """
        factories = self.factoryRepository.get_all()
        data = FactoryMapper(factories).mapper()                
        return data

    def get_active_factory(self):
        """
        """
        factories = self.factoryRepository.get_active_factory()
        data = FactoryMapper(factories).mapper()                
        return data            

    def get_by_id(self, id: int):
        """
        """
        factory = self.factoryRepository.get_by_id(id)
        data = FactoryMapper(factory).mapper()                
        return data
    
