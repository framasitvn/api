from repository.reportRepository import ReportRepository
from models.customerCatalogModel  import CustomerCatalogMapper

class ReportService():    

    def __init__(self):
        """
        """        
        self.reportRepository = ReportRepository()

    def get_customer_category(self, cust_catalog_params):
        """
        """
        cust_catalog = self.reportRepository.get_customer_category(cust_catalog_params)        
        return cust_catalog


    
