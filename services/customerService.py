from repository.customerRepository import CustomerRepository
from entity.customerEntity  import CustomerMapper

class CustomerService():
    

    def __init__(self):
        """
        """        
        self.customerRepository = CustomerRepository()

    def get_all(self):
        """
        """
        customers = self.customerRepository.get_all()
        data = CustomerMapper(customers).mapper()                
        return data
        

    def get_by_id(self, id: int):
        """
        """
        customer = self.customerRepository.get_by_id(id)
        data = CustomerMapper(customer).mapper()                
        return data
    
