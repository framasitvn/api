from repository.projectRepository import ProjectRepository
from entity.projectEntity  import ProjectMapper

class ProjectService():
    

    def __init__(self):
        """
        """        
        self.projectRepository = ProjectRepository()

    def get_all(self):
        """
        """
        projects = self.projectRepository.get_all()
        data = ProjectMapper(projects).mapper()                
        return data
    
    def get_by_factory_id(self, factory_id: int):
        """
        """
        projects = self.projectRepository.get_by_factory_id(factory_id)
        data = ProjectMapper(projects).mapper()                
        return data
         
    def get_by_id(self, id: int):
        """
        """
        project = self.projectRepository.get_by_id(id)
        data = ProjectMapper(project).mapper()                
        return data
    
