from repository.seasonRepository import SeasonRepository

class SeasonService():
    
    def __init__(self):
        """
        """        
        self.seasonRepository = SeasonRepository()
    
    def get_all(self):
        """
        """
        return self.seasonRepository.get_all()
        
    def get_by_id(self, id:int):        
        """
        """
        return self.seasonRepository.get_by_id(id)
        
        
    def add_season(self, season):   
        """
        """
        with self.conn.Connect as connect:
            season_id = self.seasonRepository.insert(season)
            connect.commit()
            data = self.get_by_id(season_id[0])
            connect.close()
        return data

    
    def update_season(self, season):
        """
        """
        with self.conn.Connect as connect:
            season_id = self.seasonRepository.update(season)
            connect.commit()
            data = self.get_by_id(season_id[0])
            connect.close()
        return data

    def delete_season(self, season):
        """
        """
        with self.conn.Connect as connect:
            data =  self.seasonRepository.delete(season)
            connect.commit()
            connect.close()
        return not isinstance(data, Exception)
    
   