from repository.categoryRepository import CategoryRepository
from entity.categoryEntity  import CategoryMapper


class CategoryService():
    

    def __init__(self):
        """
        """        
        self.categoryRepository = CategoryRepository()

    def get_all(self):
        """
        """
        categories = self.categoryRepository.get_all()
        data = CategoryMapper(categories).mapper()                
        return data
        

    def get_by_id(self, id: int):
        """
        """
        category = self.categoryRepository.get_by_id(id)
        data = CategoryMapper(category).mapper()                
        return data
    
