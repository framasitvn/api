from repository.componentRepository import ComponentRepository

class ComponentService():
    
    def __init__(self):
        """
        """        
        self.componentRepository = ComponentRepository()
        
    
    def get_all(self):
        """
        """
        return self.componentRepository.get_all()
        
    def get_by_id(self, id:int):        
        """
        """
        return self.componentRepository.get_by_id(id)
        
    def add_component(self, component):   
        """
        """
        component_id = self.componentRepository.insert(component)
        data = self.get_by_id(component_id[0])        
        return data
    
    def update_component(self, component):
        """
        """
        component_id = self.componentRepository.update(component)            
        data = self.get_by_id(component_id[0])            
        return data

    def delete_component(self, component):
        """
        """
        data =  self.componentRepository.delete(component)            
        return not isinstance( data, Exception)
    
   