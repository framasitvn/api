from repository.productGroupRepository import ProductGroupRepository
from entity.productGroupEntity  import ProductGroupMapper

class ProductGroupService():
    

    def __init__(self):
        """
        """        
        self.productGroupRepository = ProductGroupRepository()

    def get_all(self):
        """
        """
        product_groups = self.productGroupRepository.get_all()
        data = ProductGroupMapper(product_groups).mapper()                
        return data
        

    def get_by_id(self, id: int):
        """
        """
        product_group = self.productGroupRepository.get_by_id(id)
        data = ProductGroupMapper(product_group).mapper()                
        return data
    
