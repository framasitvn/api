from repository.toolingNoRepository import ToolingNoRepository
from entity.toolingNoEntity  import ToolingNoMapper

class ToolingNoService():
    

    def __init__(self):
        """
        """        
        self.toolingNoRepository = ToolingNoRepository()

    def get_all(self):
        """
        """
        tooling_numbers = self.toolingNoRepository.get_all()
        data = ToolingNoMapper(tooling_numbers).mapper()                
        return data
        

    def get_by_id(self, id: int):
        """
        """
        tooling_number = self.toolingNoRepository.get_by_id(id)
        data = ToolingNoMapper(tooling_number).mapper()                
        return data
    
