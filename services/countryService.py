from repository.countryRepository import CountryRepository
from entity.countryEntity  import CountryMapper

class CountryService():
    

    def __init__(self):
        """
        """        
        self.countryRepository = CountryRepository()

    def get_all(self):
        """
        """
        countries = self.countryRepository.get_all()
        data = CountryMapper(countries).mapper()                
        return data
        

    def get_by_id(self, id: int):
        """
        """
        country = self.countryRepository.get_by_id(id)
        data = CountryMapper(country).mapper()                
        return data
    
