from entity.baseEntity import  BaseEntity, DataEntity
from mapper.object_mapper import ObjectMapper

class ComponentEntity(BaseEntity):
    
    def __init__(self , code:str = '', name:str='', is_active:bool = False):
        super().__init__()
        self.Code = code
        self.Name = name
        self.IsActive = is_active


class ComponentMapper():

    def __init__(self, entity):        
        self.mapper_entity = entity       

    def mapper(self):
        entity = self.mapper_entity
        
        def mapping(entity):
            dt = DataEntity(entity)
            mapper = ObjectMapper()
            mapper.create_map(DataEntity, ComponentEntity, { 'Id': lambda de : de.CP_COMP_ID, 
                                                             'Code' : lambda de : de.CP_CODE, 
                                                             'Name' : lambda de : de.CP_NAME,
                                                             'IsActive' : lambda de : de.CP_IS_ACTIVE,
                                                             'CreatedBy' : lambda de : de.CP_CRE_BY,
                                                             'CreatedDate' : lambda de : de.CP_CRE_DATE,
                                                             'UpdatedBy' : lambda de : de.CP_UPD_BY,
                                                             'UpdatedDate' : lambda de : de.CP_UPD_DATE})            
            data  = mapper.map(dt, ComponentEntity)            
            return data.__dict__
        
        if isinstance(entity, list) and len(entity) > 0:
            return [mapping(value) for value in entity]
        elif isinstance(entity, dict):
            return dict(mapping(value) for value in entity.items())
        else:
            return entity
        
   
   