from entity.baseEntity import  BaseEntity, DataEntity
from mapper.object_mapper import ObjectMapper
import datetime

class ProjectEntity(BaseEntity):
    
    def __init__(self, row_id:int = None, number:int = None, code:str = None, name:str= None, molds:str = None, 
                        factory_id:int = None, development_date: datetime = None, mold_making_date:datetime= None, mass_production_date:datetime = None,
                        description:str = None, remarks: str = None):
        super().__init__()
        self.RowId = row_id
        self.Number = number
        self.Code = code
        self.Name = name
        self.Molds = molds
        self.FactoryId = factory_id
        self.DevelopmentDate = development_date
        self.MoldMakingDate = mold_making_date
        self.MassProductionDate = mass_production_date
        self.Description = description
        self.Remarks = remarks
        
class ProjectMapper():

    def __init__(self, entity):        
        self.mapper_entity = entity       

    def mapper(self):
        entity = self.mapper_entity
        
        def mapping(entity):
            dt = DataEntity(entity)
            mapper = ObjectMapper()
            mapper.create_map(DataEntity, ProjectEntity, { 'Id': lambda de : de.DP_PROJECT_KEY, 
                                                           'RowId': lambda de : de.DP_PROJECT_ID, 
                                                           'Number': lambda de : de.DP_PROJECT_NO, 
                                                           'Code': lambda de : de.DP_PROJECT_CODE, 
                                                           'Name' : lambda de : de.DP_PROJECT_NAME,
                                                           'Molds': lambda de : de.DP_PROJECT_MOLDS,
                                                           'FactoryId': lambda de : de.DP_DF_FACTORY_KEY, 
                                                           'DevelopmentDate': lambda de : de.DP_PRJ_DEV_DATE, 
                                                           'MoldMakingDate': lambda de : de.DP_PRJ_MOLD_MAKING_DATE,
                                                           'MassProductionDate': lambda de : de.DP_PRJ_MASS_PROD_DATE,
                                                           'Description': lambda de : de.DP_PRJ_DESCRIPTION,
                                                           'Remarks': lambda de : de.DP_PRJ_REMARKS })            
            data  = mapper.map(dt, ProjectEntity)            
            return data.__dict__
        
        if isinstance(entity, list) and len(entity) > 0:
            return [mapping(value) for value in entity]
        elif isinstance(entity, dict):
            return dict(mapping(value) for value in entity.items())
        else:
            return entity
        
   
   