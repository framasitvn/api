import datetime
from entity.baseEntity import  BaseEntity, DataEntity
from mapper.object_mapper import ObjectMapper

class ContactInfoEntity(BaseEntity):
    
    def __init__(self , factory_id:int = None, title:str=None, first_name:str=None, last_name:str=None, email:str=None, 
                        phone_no:str=None, phone_no_2:str=None,  is_active:bool = False):
        super().__init__()
        self.FactoryId = factory_id
        self.Title = title
        self.FirstName = first_name
        self.LastName = last_name
        self.Email = email
        self.PhoneNo = phone_no
        self.PhoneNo2 = phone_no_2
        self.IsActive = is_active


class ContactInfoMapper():

    def __init__(self, entity):        
        self.mapper_entity = entity       

    def mapper(self):
        entity = self.mapper_entity
        
        def mapping(entity):
            dt = DataEntity(entity)
            mapper = ObjectMapper()
            mapper.create_map(DataEntity, ContactInfoEntity, { 'Id': lambda de : de.CTI_CONTACT_ID, 
                                                             'FactoryId' : lambda de : de.CTI_FACTORY_ID, 
                                                             'Title' : lambda de : de.CTI_TITLE,
                                                             'FirstName' : lambda de : de.CTI_FIRST_NAME,
                                                             'LastName' : lambda de : de.CTI_LAST_NAME,
                                                             'Email' : lambda de : de.CTI_EMAIL,
                                                             'PhoneNo' : lambda de : de.CTI_PHONE_NO,
                                                             'PhoneNo2' : lambda de : de.CTI_PHONE_NO_2,
                                                             'IsActive' : lambda de : de.CTI_IS_ACTIVE,
                                                             'CreatedBy' : lambda de : de.CTI_CRE_BY,
                                                             'CreatedDate' : lambda de : de.CTI_CRE_DATE,
                                                             'UpdatedBy' : lambda de : de.CTI_UPD_BY,
                                                             'UpdatedDate' : lambda de : de.CTI_UPD_DATE})            
            data  = mapper.map(dt, ContactInfoEntity)            
            return data.__dict__
        
        if isinstance(entity, list) and len(entity) > 0:
            return [mapping(value) for value in entity]
        elif isinstance(entity, dict):
            return dict(mapping(value) for value in entity.items())
        else:
            return entity
        
   
   