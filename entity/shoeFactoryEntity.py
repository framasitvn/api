from entity.baseEntity import  BaseEntity, DataEntity
from mapper.object_mapper import ObjectMapper

class ShoeFactoryEntity(BaseEntity):
    
    def __init__(self, code:str = None, name:str= None, location:str = None, customer_hold_company: str = None):
        super().__init__()
        self.Code = code
        self.Name = name
        self.Location = location
        self.CustomerHoldCompany = customer_hold_company
        
class ShoeFactoryMapper():

    def __init__(self, entity):        
        self.mapper_entity = entity       

    def mapper(self):
        entity = self.mapper_entity
        
        def mapping(entity):
            dt = DataEntity(entity)
            mapper = ObjectMapper()
            mapper.create_map(DataEntity, ShoeFactoryEntity, { 'Id': lambda de : de.DSF_SHOE_FAC_KEY, 
                                                           'Code' : lambda de : de.DSF_SHOE_FAC_CODE, 
                                                           'Name' : lambda de : de.DSF_SHOE_FAC_NAME,
                                                           'Location' : lambda de : de.DSF_SHOE_FAC_LOCATION, 
                                                           'CustomerHoldCompany' : lambda de : de.DSF_CUST_HOLD_COMP})            

            
            data  = mapper.map(dt, ShoeFactoryEntity)            
            return data.__dict__
        
        if isinstance(entity, list) and len(entity) > 0:
            return [mapping(value) for value in entity]
        elif isinstance(entity, dict):
            return dict(mapping(value) for value in entity.items())
        else:
            return entity
        
   
   