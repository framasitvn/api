from entity.baseEntity import DataEntity, BaseEntity
from mapper.object_mapper import ObjectMapper

class ProductSeasonEntity(BaseEntity):
    
    def __init__(self,  factory_id: int= None, factory_name: str = None, 
                        project_id: int= None, project_code: str = None, project_name: str= None,
                        customer_id: int= None, customer_name: str = None,
                        country_id: int=None, domestic_import: str = None,
                        product_group_id: int=None, product_group_name: str = None,
                        tooling_id: int= None, tooling_no: str = None,
                        category_id: int=None,  category_name: str = None,
                        size_id: int=None, size_name: str = None,
                        season_start: int=None, season_start_code: str = None,season_start_name: str = None,
                        season_for: int=None,  season_for_code: str = None, season_for_name: str = None,
                        shoe_factory_id: int=None, shoe_factory_code: str = None, shoe_factory_name: str = None,
                        lead_times: str= None, is_decorate: bool = None, seasonal_forecast: float= None, max_month_forecast: float= None, 
                        max_week_forecast:float= None, max_month_ppic:float = None, max_week_ppic:float = None, colours_amount: int=None, sizes_amount: int=None, 
                        molds_amount: int=None, part_weight_runner:float= None, cbd_total_shot_weight:float= None, total_shot_weight:float= None, 
                        steps_amount:int = None, cleat_material: str=None, prices= None, components = None, images= None, shoe_factories = None):
        super().__init__()
        self.FactoryId = factory_id
        self.FactoryName = factory_name
        self.ProjectId = project_id
        self.ProjectId = project_id
        self.ProjectId = project_id
        self.CustomerId = customer_id
        self.CustomerName = customer_name
        self.CountryId = country_id
        self.DomesticImport = domestic_import
        self.ProductGroupId = product_group_id
        self.ProductGroupName = product_group_name
        self.ToolingId = tooling_id
        self.ToolingNo = tooling_no
        self.CategoryId = category_id
        self.CategoryName = category_name
        self.SizeId = size_id
        self.SizeName = size_name
        self.SeasonStart = season_start
        self.SeasonStartCode = season_start_code
        self.SeasonStartName = season_start_name
        self.SeasonFor = season_for
        self.SeasonForCode = season_for_code
        self.SeasonForName = season_for_name
        self.ShoeFactoryId = shoe_factory_id
        self.ShoeFactoryCode = shoe_factory_code
        self.ShoeFactoryName = shoe_factory_name
        self.LeadTimes = lead_times
        self.IsDecoration = is_decorate
        self.SeasonalForecast = seasonal_forecast
        self.MaxMonthForecast = max_month_forecast
        self.MaxWeekForecast = max_week_forecast
        self.MaxMonthPpic = max_month_ppic
        self.MaxWeekPpic = max_week_ppic
        self.ColoursAmount = colours_amount
        self.SizesAmount = sizes_amount
        self.MoldsAmount = molds_amount
        self.PartWeightRunner = part_weight_runner
        self.CbdTotalShotWeight = cbd_total_shot_weight
        self.TotalShotWeight = total_shot_weight
        self.StepsAmount = steps_amount
        self.CleatMaterial = cleat_material
        #list objects input 
        self.Prices = prices
        self.Components = components
        self.Images = images
        self.ShoeFactories = shoe_factories


class ProductSeasonMapper():

    def __init__(self, entity):        
        self.mapper_entity = entity       

    def mapper(self):
        entity = self.mapper_entity
        
        def mapping(entity):
            dt = DataEntity(entity)
            mapper = ObjectMapper()
            mapper.create_map(DataEntity, ProductSeasonEntity, { 'Id': lambda de : de.PS_PROD_SEASON_ID, 
                                                           'FactoryId' : lambda de : de.PS_FACTORY_ID, 
                                                           'FactoryName' : lambda de : de.DF_FAC_NAME, 
                                                           'CountryId' : lambda de : de.PS_CT_COUNTRY_ID,
                                                           'DomesticImport' : lambda de : de.DOMESTIC_IMP,
                                                           'ProjectId' : lambda de : de.PS_PROJECT_ID,
                                                           'ProjectCode' : lambda de : de.DP_PROJECT_CODE,
                                                           'ProjectName' : lambda de : de.DP_PROJECT_NAME,
                                                           'CustomerId' : lambda de : de.DC_CUST_KEY,
                                                           'CustomerName' : lambda de : de.DC_CUST_NAME,
                                                           'ProductGroupId' : lambda de : de.DPG_PROD_GROUP_KEY,
                                                           'ProductGroupName' : lambda de : de.DPG_PROD_GROUP_NAME,
                                                           'ToolingId' : lambda de : de.DTN_TOOLING_KEY,
                                                           'ToolingNo' : lambda de : de.DTN_TOOLING_NAME,
                                                           'CategoryId' : lambda de : de.DC_CATEGORY_KEY,
                                                           'CategoryName' : lambda de : de.DC_CATEGORY_NAME,
                                                           'SizeId' : lambda de : de.DS_SIZE_KEY,
                                                           'SizeName' : lambda de : de.DS_SIZE_NAME,
                                                           'SeasonStart' : lambda de : de.PS_SS_SEASON_ID_STARTING,
                                                           'SeasonStartCode' : lambda de : de.SS_START_CODE,
                                                           'SeasonStartName' : lambda de : de.SS_START_NAME,
                                                           'SeasonFor' : lambda de : de.PS_SS_SEASON_ID_FOR,
                                                           'SeasonForCode' : lambda de : de.SS_FOR_CODE,
                                                           'SeasonForName' : lambda de : de.SS_FOR_NAME,
                                                           'ShoeFactoryId' : lambda de : de.PS_SF_SHOE_FACTORY_ID,
                                                           'ShoeFactoryCode' : lambda de : de.DSF_SHOE_FAC_CODE,
                                                           'ShoeFactoryName' : lambda de : de.DSF_SHOE_FAC_NAME,
                                                           'LeadTimes' : lambda de : de.PS_LEAD_TIMES,
                                                           'IsDecoration' : lambda de : de.PS_IS_DECORATION,
                                                           'SeasonalForecast' : lambda de : de.PS_SEASONAL_FORECAST,
                                                           'MaxMonthForecast' : lambda de : de.PS_MAX_MONTH_25SF,
                                                           'MaxWeekForecast' : lambda de : de.PS_MAX_WEEK_25SF,
                                                           'MaxMonthPpic' : lambda de : de.PS_MAX_MONTH_PPIC,
                                                           'MaxWeekPpic' : lambda de : de.PS_MAX_WEEK_25PPIC,
                                                           'ColoursAmount' : lambda de : de.PS_COLOURS_AMOUNT,
                                                           'SizesAmount' : lambda de : de.PS_SIZES_AMOUNT,
                                                           'MoldsAmount' : lambda de : de.PS_MOLDS_AMOUNT,
                                                           'PartWeightRunner' : lambda de : de.PS_PARTWEIGHT_RUNNER,
                                                           'CbdTotalShotWeight' : lambda de : de.PS_CBD_TOTAL_SHOTWEIGHT,
                                                           'TotalShotWeight' : lambda de : de.PS_TOTAL_SHOTWEIGHT,
                                                           'StepsAmount' : lambda de : de.PS_STEPS_AMOUNT,
                                                           'CleatMaterial' : lambda de : de.PS_CLEAT_MATERIAL,

                                                           'CreatedBy' : lambda de : de.PS_CRE_BY,
                                                           'CreatedDate' : lambda de : de.PS_CRE_DATE,
                                                           'UpdatedBy' : lambda de : de.PS_UPD_BY,
                                                           'UpdatedDate' : lambda de : de.PS_UPD_DATE
                                                           })                                                                    
            data  = mapper.map(dt, ProductSeasonEntity)            
            return data.__dict__
        
        if isinstance(entity, list) and len(entity) > 0:
            return [mapping(value) for value in entity]
        elif isinstance(entity, dict):
            return dict(mapping(value) for value in entity.items())
        else:
            return entity
        
   
   