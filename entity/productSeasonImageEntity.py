from entity.baseEntity import  BaseEntity, DataEntity
from mapper.object_mapper import ObjectMapper

class ProductSeasonImageEntity(BaseEntity):
    
    def __init__(self , product_season_id:int = None, document_id: int= None, file_name: str= None, file_extend: str=None, path: str= None):
        super().__init__()
        
        self.ProductSeasonId = product_season_id
        self.DocumentId = document_id
        self.FileName = file_name
        self.FileExtend = file_extend
        self.Path = path

class ProductSeasonImageMapper():

    def __init__(self, entity):        
        self.mapper_entity = entity       

    def mapper(self):
        entity = self.mapper_entity
        
        def mapping(entity):
            dt = DataEntity(entity)
            mapper = ObjectMapper()
            mapper.create_map(DataEntity, ProductSeasonImageEntity, { 'Id': lambda de : de.IMG_ID, 
                                                             'ProductSeasonId' : lambda de : de.IMG_PS_PROD_SEASON_ID, 
                                                             'DocumentId' : lambda de : de.IMG_DOCUMENT_ID,
                                                             'FileName' : lambda de : de.IMG_FILE_NAME,
                                                             'FileExtend' : lambda de : de.IMG_FILE_EXTENSION,
                                                             'Path' : lambda de : de.IMG_PATH,
                                                             'CreatedBy' : lambda de : de.IMG_CRE_BY,
                                                             'CreatedDate' : lambda de : de.IMG_CRE_DATE,
                                                             'UpdatedBy' : lambda de : de.IMG_UPD_BY,
                                                             'UpdatedDate' : lambda de : de.IMG_UPD_DATE})            
            data  = mapper.map(dt, ProductSeasonImageEntity)            
            return data.__dict__
        
        if isinstance(entity, list) and len(entity) > 0:
            return [mapping(value) for value in entity]
        elif isinstance(entity, dict):
            return dict(mapping(value) for value in entity.items())
        else:
            return entity
        
   
   