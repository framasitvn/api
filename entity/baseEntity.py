from datetime import datetime
from decimal import Decimal

class BaseEntity:
    
    def __init__(self, id: int = 0, created_by: str = None, created_date: datetime = None, updated_by:str = None, updated_date: datetime = None):
        self.Id = id
        self.CreatedBy = created_by
        self.CreatedDate = created_date
        self.UpdatedBy = updated_by
        self.UpdatedDate = updated_date
    
   

class DataEntity(object):
    
    def __init__(self, data):        
        def convert_decimal_to_float(value):
            if isinstance(value, Decimal) :
                return float(value)
            else:
                return value

        for key, value in data.items():
            if isinstance(value, (list, tuple)):
               setattr(self, key, [DataEntity(e) if isinstance(e, dict) else convert_decimal_to_float(e) for e in value])
            else:
               setattr(self, key, DataEntity(key) if isinstance(key, dict) else convert_decimal_to_float(value))
    