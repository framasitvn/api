from entity.baseEntity import  BaseEntity, DataEntity
from mapper.object_mapper import ObjectMapper

class ProductGroupEntity(BaseEntity):
    
    def __init__(self, name:str= None):
        super().__init__()
        self.Name = name
        
class ProductGroupMapper():

    def __init__(self, entity):        
        self.mapper_entity = entity       

    def mapper(self):
        entity = self.mapper_entity
        
        def mapping(entity):
            dt = DataEntity(entity)
            mapper = ObjectMapper()
            mapper.create_map(DataEntity, ProductGroupEntity, { 'Id': lambda de : de.DPG_PROD_GROUP_KEY, 
                                                           'Name' : lambda de : de.DPG_PROD_GROUP_NAME })            
            data  = mapper.map(dt, ProductGroupEntity)            
            return data.__dict__
        
        if isinstance(entity, list) and len(entity) > 0:
            return [mapping(value) for value in entity]
        elif isinstance(entity, dict):
            return dict(mapping(value) for value in entity.items())
        else:
            return entity
        
   
   