from entity.baseEntity import  BaseEntity, DataEntity
from mapper.object_mapper import ObjectMapper

class CustomerEntity(BaseEntity):
    
    def __init__(self , name:str='', address:str =None, phone:str = None, email:str = None):
        super().__init__()
        self.Name = name
        self.Address = address
        self.Phone = phone
        self.Email = email
        
class CustomerMapper():

    def __init__(self, entity):        
        self.mapper_entity = entity       

    def mapper(self):
        entity = self.mapper_entity
        
        def mapping(entity):
            dt = DataEntity(entity)
            mapper = ObjectMapper()
            mapper.create_map(DataEntity, CustomerEntity, { 'Id': lambda de : de.DC_CUST_KEY, 
                                                           'Name' : lambda de : de.DC_CUST_NAME,
                                                           'Address' : lambda de : de.DC_CUST_ADDRESS,
                                                           'Phone' : lambda de : de.DC_CUST_PHONE, 
                                                           'Email' : lambda de : de.DC_CUST_EMAIL })            
            data  = mapper.map(dt, CustomerEntity)            
            return data.__dict__
        
        if isinstance(entity, list) and len(entity) > 0:
            return [mapping(value) for value in entity]
        elif isinstance(entity, dict):
            return dict(mapping(value) for value in entity.items())
        else:
            return entity
        
   
   