from entity.baseEntity import  BaseEntity, DataEntity
from mapper.object_mapper import ObjectMapper

class ProductSeasonShoeFactoryEntity(BaseEntity):
    
    def __init__(self , product_season_id:int = None, shoe_factory_id:int = None):
        super().__init__()
        self.ProductSeasonId = product_season_id
        self.ShoeFactoryId = shoe_factory_id
        
class ProductSeasonShoeFactoryMapper():

    def __init__(self, entity):        
        self.mapper_entity = entity       

    def mapper(self):
        entity = self.mapper_entity
        
        def mapping(entity):
            dt = DataEntity(entity)
            mapper = ObjectMapper()
            mapper.create_map(DataEntity, ProductSeasonShoeFactoryEntity, { 'Id': lambda de : de.PSSF_SHOE_FAC_ID, 
                                                             'ProductSeasonId' : lambda de : de.PSSF_PS_PROD_SEASON_ID, 
                                                             'ShoeFactoryId' : lambda de : de.PSSF_SF_SHOE_FACTORY_ID,
                                                             'CreatedBy' : lambda de : de.PSSF_CRE_BY,
                                                             'CreatedDate' : lambda de : de.PSSF_CRE_DATE,
                                                             'UpdatedBy' : lambda de : de.PSSF_UPD_BY,
                                                             'UpdatedDate' : lambda de : de.PSSF_UPD_DATE})            
            data  = mapper.map(dt, ProductSeasonShoeFactoryEntity)            
            return data.__dict__
        
        if isinstance(entity, list) and len(entity) > 0:
            return [mapping(value) for value in entity]
        elif isinstance(entity, dict):
            return dict(mapping(value) for value in entity.items())
        else:
            return entity
        
   
   