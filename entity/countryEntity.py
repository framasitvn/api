from entity.baseEntity import  BaseEntity, DataEntity
from mapper.object_mapper import ObjectMapper

class CountryEntity():
    
    def __init__(self , id:int = None, code:str = None, name:str= None):
        self.Id = id
        self.Code = code
        self.Name = name
        
class CountryMapper():

    def __init__(self, entity):        
        self.mapper_entity = entity       

    def mapper(self):
        entity = self.mapper_entity
        
        def mapping(entity):
            dt = DataEntity(entity)
            mapper = ObjectMapper()
            mapper.create_map(DataEntity, CountryEntity, { 'Id': lambda de : de.CT_COUNTRY_ID, 
                                                            'Code' : lambda de : de.CT_CODE,
                                                            'Name' : lambda de : de.CT_NAME 
                                                        })            
            data  = mapper.map(dt, CountryEntity)
            return data.__dict__
        
        if isinstance(entity, list) and len(entity) > 0:
            return [mapping(value) for value in entity]
        elif isinstance(entity, dict):
            return dict(mapping(value) for value in entity.items())
        else:
            return entity
        
   
   