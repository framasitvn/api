from entity.baseEntity import  BaseEntity, DataEntity
from mapper.object_mapper import ObjectMapper

class CategoryEntity(BaseEntity):
    
    def __init__(self , name:str=''):
        super().__init__()
        self.Name = name
        
class CategoryMapper():

    def __init__(self, entity):        
        self.mapper_entity = entity       

    def mapper(self):
        entity = self.mapper_entity
        
        def mapping(entity):
            dt = DataEntity(entity)
            mapper = ObjectMapper()
            mapper.create_map(DataEntity, CategoryEntity, { 'Id': lambda de : de.DC_CATEGORY_KEY, 
                                                           'Name' : lambda de : de.DC_CATEGORY_NAME })            
            data  = mapper.map(dt, CategoryEntity)            
            return data.__dict__
        
        if isinstance(entity, list) and len(entity) > 0:
            return [mapping(value) for value in entity]
        elif isinstance(entity, dict):
            return dict(mapping(value) for value in entity.items())
        else:
            return entity
        
   
   