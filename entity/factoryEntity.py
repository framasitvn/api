from entity.baseEntity import  BaseEntity, DataEntity
from mapper.object_mapper import ObjectMapper

class FactoryEntity(BaseEntity):
    
    def __init__(self , code:str = '', name:str='', is_active:bool = False, region:str = '', country:str = ''):
        super().__init__()
        self.Code = code
        self.Name = name
        self.IsActive = is_active
        self.Region = region
        self.Country = country

class FactoryMapper():

    def __init__(self, entity):        
        self.mapper_entity = entity       

    def mapper(self):
        entity = self.mapper_entity
        
        def mapping(entity):
            dt = DataEntity(entity)
            mapper = ObjectMapper()
            mapper.create_map(DataEntity, FactoryEntity, { 'Id': lambda de : de.DF_FACTORY_KEY, 
                                                           'Code' : lambda de : de.DF_FAC_CODE, 
                                                           'Name' : lambda de : de.DF_FAC_NAME,
                                                           'IsActive' : lambda de : de.DF_FAC_IS_ACTIVE,
                                                           'Region' : lambda de : de.DF_FAC_REGION,
                                                           'Country' : lambda de : de.DF_FAC_COUNTRY})            
            data  = mapper.map(dt, FactoryEntity)            
            return data.__dict__
        
        if isinstance(entity, list) and len(entity) > 0:
            return [mapping(value) for value in entity]
        elif isinstance(entity, dict):
            return dict(mapping(value) for value in entity.items())
        else:
            return entity
        
   
   