import datetime
from entity.baseEntity import  BaseEntity, DataEntity
from mapper.object_mapper import ObjectMapper

class SeasonEntity(BaseEntity):
    
    def __init__(self , code:str = '', name:str='', start_date: datetime= None, end_date: datetime = None,  is_active:bool = False):
        super().__init__()
        self.Code = code
        self.Name = name
        self.StartDate = start_date
        self.EndDate = end_date
        self.IsActive = is_active


class SeasonMapper():

    def __init__(self, entity):        
        self.mapper_entity = entity       

    def mapper(self):
        entity = self.mapper_entity
        
        def mapping(entity):
            dt = DataEntity(entity)
            mapper = ObjectMapper()
            mapper.create_map(DataEntity, SeasonEntity, { 'Id': lambda de : de.SS_SEASON_ID, 
                                                             'Code' : lambda de : de.SS_CODE, 
                                                             'Name' : lambda de : de.SS_NAME,
                                                             'StartDate' : lambda de : de.SS_START_DATE,
                                                             'EndDate' : lambda de : de.SS_END_DATE,
                                                             'IsActive' : lambda de : de.SS_IS_ACTIVE,
                                                             'CreatedBy' : lambda de : de.SS_CRE_BY,
                                                             'CreatedDate' : lambda de : de.SS_CRE_DATE,
                                                             'UpdatedBy' : lambda de : de.SS_UPD_BY,
                                                             'UpdatedDate' : lambda de : de.SS_UPD_DATE})            
            data  = mapper.map(dt, SeasonEntity)            
            return data.__dict__
        
        if isinstance(entity, list) and len(entity) > 0:
            return [mapping(value) for value in entity]
        elif isinstance(entity, dict):
            return dict(mapping(value) for value in entity.items())
        else:
            return entity
        
   
   