from entity.baseEntity import  BaseEntity, DataEntity
from mapper.object_mapper import ObjectMapper

class ProductSeasonPriceEntity(BaseEntity):
    
    def __init__(self , index:int = None, value:float = None, product_season_id:int= None, description:str=None, is_main_price:bool = False):
        super().__init__()
        self.Index = index
        self.Value = value
        self.ProductSeasonId = product_season_id
        self.Description = description
        self.IsMainPrice = is_main_price


class ProductSeasonPriceMapper():

    def __init__(self, entity):        
        self.mapper_entity = entity       

    def mapper(self):
        entity = self.mapper_entity
        
        def mapping(entity):
            dt = DataEntity(entity)
            mapper = ObjectMapper()
            mapper.create_map(DataEntity, ProductSeasonPriceEntity, { 'Id': lambda de : de.PSP_PRICE_ID, 
                                                             'Index' : lambda de : de.PSP_PRICE_INDEX, 
                                                             'Value' : lambda de : de.PSP_PRICE_VALUE,
                                                             "ProductSeasonId": lambda de : de.PSP_PS_PROD_SEASON_ID,
                                                             'Description' : lambda de : de.PSP_DESCRIPTION,
                                                             'IsMainPrice' : lambda de : de.PSP_IS_MAIN_PRICE,
                                                             'CreatedBy' : lambda de : de.PSP_CRE_BY,
                                                             'CreatedDate' : lambda de : de.PSP_CRE_DATE,
                                                             'UpdatedBy' : lambda de : de.PSP_UPD_BY,
                                                             'UpdatedDate' : lambda de : de.PSP_UPD_DATE})            
            data  = mapper.map(dt, ProductSeasonPriceEntity)            
            return data.__dict__
        
        if isinstance(entity, list) and len(entity) > 0:
            return [mapping(value) for value in entity]
        elif isinstance(entity, dict):
            return dict(mapping(value) for value in entity.items())
        else:
            return entity
        
   
   