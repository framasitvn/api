from entity.baseEntity import  BaseEntity, DataEntity
from mapper.object_mapper import ObjectMapper

class ProductSeasonComponentEntity(BaseEntity):
    
    def __init__(self , component_id:int = None, product_season_id: int = None, index:int = None, shot_weight:float = None, main_material:str=None, 
                        alt_mat_trans: str= None, alt_mat_black: str=None):
        super().__init__()
        self.ComponentId = component_id
        self.ProductSeasonId = product_season_id
        self.Index = index
        self.ShotWeight = shot_weight
        self.MainMaterial = main_material
        self.AltMatTrans = alt_mat_trans
        self.AltMatBlack = alt_mat_black


class ProductSeasonComponentMapper():

    def __init__(self, entity):        
        self.mapper_entity = entity       

    def mapper(self):
        entity = self.mapper_entity
        
        def mapping(entity):
            dt = DataEntity(entity)
            mapper = ObjectMapper()
            mapper.create_map(DataEntity, ProductSeasonComponentEntity, { 'Id': lambda de : de.PSC_COMPONENT_ID, 
                                                             'ComponentId' : lambda de : de.PSC_CP_COMP_ID, 
                                                             'ProductSeasonId' : lambda de : de.PSC_PS_PROD_SEASON_ID,
                                                             'Index' : lambda de : de.PSC_STEP_INDEX,
                                                             'ShotWeight' : lambda de : de.PSC_SHOTWEIGHT,
                                                             'MainMaterial' : lambda de : de.PSC_MAIN_MATERIAL,
                                                             'AltMatTrans' : lambda de : de.PSC_ALT_MAT_TRANS,
                                                             'AltMatBlack' : lambda de : de.PSC_ALT_MAT_BLACK,
                                                             'CreatedBy' : lambda de : de.PSC_CRE_BY,
                                                             'CreatedDate' : lambda de : de.PSC_CRE_DATE,
                                                             'UpdatedBy' : lambda de : de.PSC_UPD_BY,
                                                             'UpdatedDate' : lambda de : de.PSC_UPD_DATE})            
            data  = mapper.map(dt, ProductSeasonComponentEntity)            
            return data.__dict__
        
        if isinstance(entity, list) and len(entity) > 0:
            return [mapping(value) for value in entity]
        elif isinstance(entity, dict):
            return dict(mapping(value) for value in entity.items())
        else:
            return entity
        
   
   