import json
from flask import jsonify

class AuthError(Exception):
    def __init__(self, error, status_code):
        self.error = error
        self.status_code = status_code

class RequestError(Exception):    
    def __init__(self, message):
        self.status_code = 400
        self.error = {"code": "Invalid_request", "description": message}

class ResponseData():
    def __init__(self, code, message, data = None):
        self.code = code
        self.message = message
        self.data = data
    
    def response(self):
        return jsonify({"code": self.code, 
                        "message": self.message,
                        "result": self.data})
        