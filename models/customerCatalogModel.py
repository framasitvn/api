from entity.baseEntity import  DataEntity
from mapper.object_mapper import ObjectMapper
import decimal

class CustomerCatalogModel():
    
    def __init__(self , product_season_id:int= None, factory_id:int = None, factory_name:str= None, project_id:int = None, project_number:str = None,
                        project_name:str = None, customer_id:int= None, customer_name:str = None, domestic_export_id:int= None, domestic_export_code:str = None,
                        domestic_export_name:str = None, product_group_id:int= None, product_group_name:str = None, tooling_id:int= None, tooling_number:str = None,
                        category_id:int= None, category_name:str = None, size_id:int= None, season_staring:int= None, season_starting_code:str = None,
                        season_starting_name:str = None, season_for:int= None, season_for_code:str = None, season_for_name:str = None, shoe_factory_id:int= None,
                        shoe_factory_code:str = None, shoe_factory_name:str = None, lead_times:str = None, is_decoration:bool =None, season_forecast:decimal = None,
                        max_month_25sf:decimal = None, max_week25_25sf:decimal = None, max_month_ppic:decimal = None, max_week25_25ppic:decimal = None,
                        colours_amount:int = None, sizes_amount:int = None, mold_amounts:int = None, part_weight_runner:decimal = None, 
                        cbd_total_shotweight:decimal = None, total_shot_weight:decimal = None, steps_amount:int = None, cleat_material:str = None):
        super().__init__()
        self.ProductSeasonId = product_season_id
        self.FactoryId = factory_id
        self.FactoryName = factory_name
        self.ProjectId = project_id
        self.ProjectNumber = project_number
        self.ProjectName = project_name
        self.CustomerId = customer_id
        self.CustomerName = customer_name
        self.DomesticExportId = domestic_export_id
        self.DomesticExportCode = domestic_export_code
        self.DomesticExportName = domestic_export_name
        self.ProductGroupId = product_group_id
        self.ProductGroupName = product_group_name
        self.ToolingId = tooling_id
        self.ToolingNumber = tooling_number
        self.CategoryId = category_id
        self.CategoryName = category_name
        self.SizeId = size_id
        self.SeasonStaring = season_staring
        self.SeasonStartCode = season_starting_code
        self.SeasonStartingName = season_starting_name
        self.SeasonFor = season_for
        self.SeasonForCode = season_for_code
        self.SeasonForName = season_for_name
        self.ShoeFactoryId = shoe_factory_id
        self.ShoeFactoryCode = shoe_factory_code
        self.ShoeFactoryName = shoe_factory_name
        self.LeadTimes = lead_times
        self.IsDecoration = is_decoration
        self.SeasonForecast = season_forecast
        self.MaxMonth25Sf = max_month_25sf
        self.MaxWeek25Sf = max_week25_25ppic
        self.MaxMonthPpic = max_month_ppic
        self.MaxWeek25Ppic = max_week25_25ppic
        self.ColoursAmount = colours_amount
        self.SizesAmount = sizes_amount
        self.MoldAmounts = mold_amounts
        self.PartWeightRunner = part_weight_runner
        self.CbdTotalShotWeight = cbd_total_shotweight
        self.TotalShotWeight = total_shot_weight
        self.StepsAmount = steps_amount
        
class CustomerCatalogMapper():

    def __init__(self, entity):        
        self.mapper_entity = entity       

    def mapper(self):
        entity = self.mapper_entity
        
        def mapping(entity):
            dt = DataEntity(entity)
            mapper = ObjectMapper()
            
            mapper.create_map(DataEntity, CustomerCatalogModel, { 'ProductSeasonId': lambda de : de.PS_PROD_SEASON_ID,
                                                                  'FactoryId': lambda de : de.PS_FACTORY_ID,
                                                                  'FactoryName': lambda de : de.FACTORY_NAME,
                                                                  'ProjectId': lambda de : de.PS_PROJECT_ID,
                                                                  'ProjectNumber': lambda de : de.ITEM_NUMBER,
                                                                  'ProjectName': lambda de : de.ITEM_NAME,
                                                                  'CustomerId': lambda de : de.PS_CUSTOMER_ID,
                                                                  'CustomerName': lambda de : de.CUSTOMER_NAME,
                                                                  'DomesticExportId': lambda de : de.PS_CT_COUNTRY_ID,
                                                                  'DomesticExportCode': lambda de : de.DOMESTIC_EXPORT_CODE,
                                                                  'DomesticExportName': lambda de : de.DOMESTIC_EXPORT_NAME,
                                                                  'ProductGroupId': lambda de : de.PS_PROD_GROUP_ID,
                                                                  'ProductGroupName': lambda de : de.PRODUCT_GROUP,
                                                                  'ToolingId': lambda de : de.PS_TOOLING_ID,
                                                                  'ToolingNumber': lambda de : de.TOOLING_NUMBER,
                                                                  'CategoryId': lambda de : de.PS_CATEGORY_ID,
                                                                  'CategoryName': lambda de : de.CATEGORY_NAME,
                                                                  'SizeId': lambda de : de.PS_SIZE_ID,
                                                                  'SeasonStaring': lambda de : de.PS_SS_SEASON_ID_STARTING,
                                                                  'SeasonStartCode': lambda de : de.SEASON_STARTING_CODE,
                                                                  'SeasonStartingName': lambda de : de.SEASON_STARTING_NAME,
                                                                  'SeasonFor': lambda de : de.PS_SS_SEASON_ID_FOR,
                                                                  'SeasonForCode': lambda de : de.SEASON_FOR_CODE,
                                                                  'SeasonForName': lambda de : de.SEASON_FOR_NAME,
                                                                  'ShoeFactoryId': lambda de : de.PS_SF_SHOE_FACTORY_ID,
                                                                  'ShoeFactoryCode': lambda de : de.SHOE_FACTORY_CODE,
                                                                  'ShoeFactoryName': lambda de : de.SHOE_FACTORY_NAME,
                                                                  'LeadTimes': lambda de : de.PS_LEAD_TIMES,
                                                                  'IsDecoration': lambda de : de.PS_IS_DECORATION,
                                                                  'SeasonForecast': lambda de : de.PS_SEASONAL_FORECAST,
                                                                  'MaxMonth25Sf': lambda de : de.PS_MAX_MONTH_25SF,
                                                                  'MaxWeek25Sf': lambda de : de.PS_MAX_WEEK_25SF,
                                                                  'MaxMonthPpic': lambda de : de.PS_MAX_MONTH_PPIC,
                                                                  'MaxWeek25Ppic': lambda de : de.PS_MAX_WEEK_25PPIC,
                                                                  'ColoursAmount': lambda de : de.PS_COLOURS_AMOUNT,
                                                                  'SizesAmount': lambda de : de.PS_SIZES_AMOUNT,
                                                                  'MoldAmounts': lambda de : de.PS_MOLDS_AMOUNT,
                                                                  'PartWeightRunner': lambda de : de.PS_PARTWEIGHT_RUNNER,
                                                                  'CbdTotalShotWeight': lambda de : de.PS_CBD_TOTAL_SHOTWEIGHT,
                                                                  'TotalShotWeight': lambda de : de.PS_TOTAL_SHOTWEIGHT,
                                                                  'StepsAmount': lambda de : de.PS_STEPS_AMOUNT,
                                                                  'CleatMaterial': lambda de : de.PS_CLEAT_MATERIAL                                                                
                                                                  })            
            data  = mapper.map(dt, CustomerCatalogModel)            
            return data.__dict__
        
        if isinstance(entity, list):
            return [mapping(value) for value in entity]
        elif isinstance(entity, dict):
            return dict(mapping(value) for value in entity.items())
        else:
            return entity
        
   
   