from flask_cors import cross_origin
from flask import request, jsonify
from app_response import AuthError, ResponseData
import json
import app_config
#from flask_restful import Api, Resource
from requestEntity.baseRequest import ApiRequest

class BaseController():

    def __init__(self):        
        self.__token_validated()
    
    def get_token_auth_header(self):
        auth = request.headers.get("Authorization", None)

        if not auth:
            raise AuthError({"code": "authorization_header_missing",
                            "description": "Authorization header is expected"}, 401)
        parts = auth.split()
        if parts[0].lower() != "bearer" :
            raise AuthError({"code": "invalid_header",
                            "description": "Authorization header must start with" " Bearer"}, 401)
        elif len(parts) == 1:
            raise AuthError({"code": "invalid_header",
                            "description": "Token not found"}, 401)
        elif len(parts) > 2 :
            raise AuthError({"code": "invalid_header",
                            "description": "Authorization header must be" " Bearer token"}, 401)
        token = parts[1]
        return token


    def response_data(self, data, code = 1, message = "OK"):
        result = ResponseData(code, message, data)
        return result.response()


    def __token_validated(self):

        def get_token_from_cache(scope = None):
            #token = app._get_token_from_cache(scope)
            #headers={'Authorization': 'Bearer ' + token['access_token']}
            token = {'access_token': 'SampleToken'}
            return 'Bearer ' + token['access_token']
            
        token = get_token_from_cache(app_config.SCOPE)
        #TODO check token expired, corrected 
        
        if not token:
            raise AuthError({"code": "Invalid_Token",
                             "description": "Token has been expired or invalid"}, 401)        

    