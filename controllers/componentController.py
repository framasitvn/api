from services.componentService import ComponentService
from controllers.baseController import BaseController
from requestEntity.componentRequest import ComponentRequest
from flask import Blueprint, jsonify, Response, request
from app_response import RequestError
from flask_cors import cross_origin
from requestEntity.baseRequest import ApiRequest, Action

componentController = Blueprint('componentController', __name__)

class ComponentController(BaseController):
    
    def __init__(self):
        super().__init__()
        self.componentService = ComponentService()


# @requires_auth
#@cross_origin(headers=["Content-Type", "Authorization"])
@componentController.route('/api/component/all', methods=['POST'])
def get_all():     
    """
    Get all components
    ---
    tags:
        - Component
    description: Get all components.
    responses:
        200:
          description: All components
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiResponse'
    """
    comp = ComponentController()    
    result = comp.componentService.get_all()         
    return comp.response_data(result)
    

@componentController.route('/api/component/<id>', methods=['POST'])
def get_by_id(id:int):
    """
    Get component by id
    ---
    description: >
        Get component by id
        getting it using the **id** parameter provided!
    tags:
        - Component
    parameters:
        - name: id
          in: path
          description: id component
          required: true
          schema:
            type: string
    responses:
        200:
          description: A component.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiResponse'
    """
    try:
        value = int(id)
    except:
        raise RequestError("id must be number")
    comp = ComponentController()
    result = comp.componentService.get_by_id(value)            
    return comp.response_data(result)
    

@componentController.route('/api/component/add', methods = ['POST'])
@cross_origin(headers=["Content-Type", "application/json"])
def add():    
    """
    Add component
    ---
    description: >
        Add component        
    tags:
        - Component
    requestBody:
        description: The component to create.    
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/ComponentRequest'
    responses:
        200:
          description: component.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiResponse'        
    """
    meta_request = request.get_json()        
    api = ApiRequest(meta_request)
    cp_request = ComponentRequest(api.Data)
    cp_request.Username = api.Username
    comp = ComponentController()
    result = comp.componentService.add_component(cp_request)
    return comp.response_data(result)
    
@componentController.route('/api/component/modify', methods = ['POST'])
@cross_origin(headers=["Content-Type", "application/json"])
def update():
    """
    Update component
    ---
    description: >
        Update component        
    tags:
        - Component
    requestBody:
        description: The component to update.    
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/UpdateComponentRequest'
    responses:
        200:
          description: component.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiResponse'        
    """
    meta_request = request.get_json()        
    api = ApiRequest(meta_request)
    cp_request = ComponentRequest(api.Data, id= 0)
    cp_request.Username = api.Username
    comp = ComponentController()
    result = comp.componentService.update_component(cp_request)
    return comp.response_data(result)
    

@componentController.route('/api/component/inactivated', methods = ['POST'])
@cross_origin(headers=["Content-Type", "application/json"])
def inactivated():
    """
    Deactivated component
    ---
    description: >
        Deactivated component        
    tags:
        - Component
    requestBody:
        description: make component to inactive.    
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/InactiveMasterRequest'
    responses:
        200:
          description: component.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiResponse'        
    """
    meta_request = request.get_json()        
    api = ApiRequest(meta_request)
    cp_request = ComponentRequest(api.Data, id= 0, action=Action.DEACTIVE)
    cp_request.Username = api.Username
    
    comp = ComponentController()
    result = comp.componentService.delete_component(cp_request)
    return comp.response_data(result)


