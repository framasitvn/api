from services.projectService import ProjectService
from flask import Blueprint, jsonify, Response, request
from app_response import RequestError
from flask_cors import cross_origin
from controllers.baseController import BaseController

projectController = Blueprint('projectController', __name__)

class ProjectController(BaseController):
    
    def __init__(self):
        super().__init__()
        self.projectService = ProjectService()

# @requires_auth
#@cross_origin(headers=["Content-Type", "Authorization"])
@projectController.route('/api/project/all', methods=['POST'])
def get_all():   
    """
    Get all projects
    ---
    tags:
        - Project
    description: Get all projects.
    responses:
        200:
          description: All projects
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiResponse'
    """
    project = ProjectController()    
    result = project.projectService.get_all()         
    return project.response_data(result)

@projectController.route('/api/project/by_factory/<factory_id>', methods=['POST'])
def get_by_factory_id(factory_id:int):
    """
    Get project by factory_id
    ---
    description: >
        Get project by factory_id
        getting it using the **factory_id** parameter provided!
    tags:
        - Project
    parameters:
        - name: factory_id
          in: path
          description: factory_id project
          required: true
          schema:
            type: string
    responses:
        200:
          description: some projects.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiResponse'
    """
    try:
        value = int(factory_id)
    except: 
        raise RequestError("factory_id must be number")
    project = ProjectController()    
    result = project.projectService.get_by_factory_id(value)         
    return project.response_data(result)


@projectController.route('/api/project/<id>', methods=['POST'])
def get_by_id(id: int):
    """
    Get project by id
    ---
    description: >
        Get project by id
        getting it using the **id** parameter provided!
    tags:
        - Project
    parameters:
        - name: id
          in: path
          description: id project
          required: true
          schema:
            type: string
    responses:
        200:
          description: A project.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiResponse'
    """
    try:
        value = int(id)
    except:
        raise RequestError("id must be number")
    project = ProjectController()    
    result = project.projectService.get_by_id(value)         
    return project.response_data(result)


