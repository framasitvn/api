from services.reportService import ReportService
from flask import Blueprint, jsonify, Response, request
from app_response import RequestError
from flask_cors import cross_origin
from controllers.baseController import BaseController
from requestEntity.baseRequest import ApiRequest
from requestEntity.customerCatalogRequest import CustomerCatalogRequest

reportController = Blueprint('reportController', __name__)

class ReportController(BaseController):
    
    def __init__(self):
        super().__init__()
        self.reportService = ReportService()

  
@reportController.route('/api/reports/custcatalog', methods=['POST'])
@cross_origin(headers=["Content-Type", "application/json"])
def get_report_customer_catalog():
    """
    Get report customer's catalogue
    ---
    description: >
        Get report customer's catalogue
    tags:
        - Reports
    requestBody:
        description: The report customer's catalogue
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/ReportCustomerCatalogRequest'
    responses:
        200:
          description: List items .
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiResponse'        
    """
    meta_request = request.get_json()        
    api = ApiRequest(meta_request)
    cust_cat_request = CustomerCatalogRequest(api.Data)    
    report = ReportController()
    result = report.reportService.get_customer_category(cust_cat_request)
    return report.response_data(result, 200)    
    