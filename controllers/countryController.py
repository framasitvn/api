from services.countryService import CountryService
from flask import Blueprint, jsonify, Response, request
from app_response import RequestError
from flask_cors import cross_origin
from controllers.baseController import BaseController

countryController = Blueprint('countryController', __name__)

class CountryController(BaseController):
    
    def __init__(self):
        super().__init__()
        self.countryService = CountryService()

# @requires_auth
#@cross_origin(headers=["Content-Type", "Authorization"])
@countryController.route('/api/country/all', methods=['POST'])
def get_all():    
    """
    Get all country
    ---
    tags:
        - Country
    description: Get all country.
    responses:
        200:
          description: All country
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiResponse'
    """
    country = CountryController()    
    result = country.countryService.get_all()         
    return country.response_data(result)

@countryController.route('/api/country/<id>', methods=['POST'])
def get_by_id(id: int):
    """
    Get country by id
    ---
    description: >
        Get country by id
        getting it using the **id** parameter provided!
    tags:
        - Country
    parameters:
        - name: id
          in: path
          description: id country
          required: true
          schema:
            type: string
    responses:
        200:
          description: A country.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiResponse'
    """
    try:
        value = int(id)
    except:
        raise RequestError("id must be number")
    country = CountryController()    
    result = country.countryService.get_by_id(value)         
    return country.response_data(result)


