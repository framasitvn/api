from services.factoryService import FactoryService
from flask import Blueprint, jsonify, Response, request
from app_response import RequestError
from flask_cors import cross_origin
from controllers.baseController import BaseController

factoryController = Blueprint('factoryController', __name__)

class FactoryController(BaseController):
    
    def __init__(self):
        super().__init__()
        self.factoryService = FactoryService()

# @requires_auth
#@cross_origin(headers=["Content-Type", "Authorization"])
@factoryController.route('/api/factory/all', methods=['POST'])
def get_all():   
    """
    Get all factories
    ---
    tags:
        - Factory
    description: Get all factories.
    responses:
        200:
          description: All factories
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiResponse'
    """
    factory = FactoryController()    
    result = factory.factoryService.get_all()         
    return factory.response_data(result)

@factoryController.route('/api/factory/<id>', methods=['POST'])
def get_by_id(id: int):
    """
    Get factory by id
    ---
    description: >
        Get factory by id
        getting it using the **id** parameter provided!
    tags:
        - Factory
    parameters:
        - name: id
          in: path
          description: id factory
          required: true
          schema:
            type: string
    responses:
        200:
          description: A factory.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiResponse'
    """
    try:
        value = int(id)
    except:
        raise RequestError("id must be number")
    factory = FactoryController()    
    result = factory.factoryService.get_by_id(value)         
    return factory.response_data(result)


