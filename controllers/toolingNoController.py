from services.toolingNoService import ToolingNoService
from flask import Blueprint, jsonify, Response, request
from app_response import RequestError
from flask_cors import cross_origin
from controllers.baseController import BaseController

toolingNoController = Blueprint('toolingNoController', __name__)

class ToolingNoController(BaseController):
    
    def __init__(self):
        super().__init__()
        self.toolingNoService = ToolingNoService()

# @requires_auth
#@cross_origin(headers=["Content-Type", "Authorization"])
@toolingNoController.route('/api/tooling/all', methods=['POST'])
def get_all():    
    """
    Get all tooling
    ---
    tags:
        - Tooling
    description: Get all tooling.
    responses:
        200:
          description: All tooling
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiResponse'
    """
    toolingNo = ToolingNoController()    
    result = toolingNo.toolingNoService.get_all()         
    return toolingNo.response_data(result)

@toolingNoController.route('/api/tooling/<id>', methods=['POST'])
def get_by_id(id: int):
    """
    Get tooling by id
    ---
    description: >
        Get tooling by id
        getting it using the **id** parameter provided!
    tags:
        - Tooling
    parameters:
        - name: id
          in: path
          description: id tooling
          required: true
          schema:
            type: string
    responses:
        200:
          description: A tooling.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiResponse'
    """
    try:
        value = int(id)
    except:
        raise RequestError("id must be number")
    toolingNo = ToolingNoController()    
    result = toolingNo.toolingNoService.get_by_id(value)         
    return toolingNo.response_data(result)


