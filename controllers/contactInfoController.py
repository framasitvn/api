from services.contactInfoService import ContactInfoService
from controllers.baseController import BaseController
from requestEntity.contactInfoRequest import ContactInfoRequest
from flask import Blueprint, jsonify, Response, request
from app_response import RequestError
from flask_cors import cross_origin
from requestEntity.baseRequest import ApiRequest, Action

contactInfoController = Blueprint('contactInfoController', __name__)

class ContactInfoController(BaseController):
    
    def __init__(self):
        super().__init__()
        self.contactInfoService = ContactInfoService()


# @requires_auth
#@cross_origin(headers=["Content-Type", "Authorization"])
@contactInfoController.route('/api/contactinfo/all', methods=['POST'])
def get_all():    
    """
    Get all contacts info
    ---
    tags:
        - Contact Info
    description: Get all contacts info.
    responses:
        200:
          description: All contacts info
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiResponse'
    """
    comp = ContactInfoController()    
    result = comp.contactInfoService.get_all()         
    return comp.response_data(result)
    

@contactInfoController.route('/api/contactinfo/<id>', methods=['POST'])
def get_by_id(id:int):
    """
    Get contact info by id
    ---
    description: >
        Get contact info by id
        getting it using the **id** parameter provided!
    tags:
        - Contact Info
    parameters:
        - name: id
          in: path
          description: id contact info
          required: true
          schema:
            type: string
    responses:
        200:
          description: A contact info.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiResponse'
    """
    try:
        value = int(id)
    except:
        raise RequestError("id must be number")
    comp = ContactInfoController()
    result = comp.contactInfoService.get_by_id(value)            
    return comp.response_data(result)
    

@contactInfoController.route('/api/contactinfo/add', methods = ['POST'])
@cross_origin(headers=["Content-Type", "application/json"])
def add():
    """
    Add contact info
    ---
    description: >
        Add contact info        
    tags:
        - Contact Info
    requestBody:
        description: The contact info to create.    
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/ContactInfoRequest'
    responses:
        200:
          description: contact info.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiResponse'        
    """    
    meta_request = request.get_json()        
    api = ApiRequest(meta_request)
    ci_request = ContactInfoRequest(api.Data)
    ci_request.Username = api.Username

    contact_info = ContactInfoController()
    result = contact_info.contactInfoService.add_contact_info(ci_request)
    return contact_info.response_data(result)
    
@contactInfoController.route('/api/contactinfo/modify', methods = ['POST'])
@cross_origin(headers=["Content-Type", "application/json"])
def update():
    """
    Update contact info
    ---
    description: >
        Update contact info        
    tags:
        - Contact Info
    requestBody:
        description: The contact info to update.    
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/UpdateContactInfoRequest'
    responses:
        200:
          description: contact info.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiResponse'        
    """
    meta_request = request.get_json()        
    api = ApiRequest(meta_request)
    ci_request = ContactInfoRequest(api.Data, id= 0)
    ci_request.Username = api.Username
    
    contact_info = ContactInfoController()
    result = contact_info.contactInfoService.update_contact_info(ci_request)
    return contact_info.response_data(result)
    

@contactInfoController.route('/api/contactinfo/inactivated', methods = ['POST'])
@cross_origin(headers=["Content-Type", "application/json"])
def inactivated():
    """
    Deactivated contact info
    ---
    description: >
        Deactivated contact info        
    tags:
        - Contact Info
    requestBody:
        description: make contact info to inactive.    
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/InactiveMasterRequest'
    responses:
        200:
          description: contact info.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiResponse'        
    """
    meta_request = request.get_json()        
    api = ApiRequest(meta_request)
    ci_request = ContactInfoRequest(api.Data, id= 0, action= Action.DEACTIVE)
    ci_request.Username = api.Username
    
    contact_info = ContactInfoController()
    result = contact_info.contactInfoService.delete_contact_info(ci_request)
    return contact_info.response_data(result)


