from services.productGroupService import ProductGroupService
from flask import Blueprint, jsonify, Response, request
from app_response import RequestError
from flask_cors import cross_origin
from controllers.baseController import BaseController

productGroupController = Blueprint('productGroupController', __name__)

class ProductGroupController(BaseController):
    
    def __init__(self):
        super().__init__()
        self.productGroupService = ProductGroupService()

# @requires_auth
#@cross_origin(headers=["Content-Type", "Authorization"])
@productGroupController.route('/api/productgroup/all', methods=['POST'])
def get_all():
    """
    Get all product group
    ---
    tags:
        - Product Group
    description: Get all product group.
    responses:
        200:
          description: All product group
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiResponse'
    """
    productGroup = ProductGroupController()    
    result = productGroup.productGroupService.get_all()         
    return productGroup.response_data(result)

@productGroupController.route('/api/productgroup/<id>', methods=['POST'])
def get_by_id(id: int):
    """
    Get product group by id
    ---
    description: >
        Get product group by id
        getting it using the **id** parameter provided!
    tags:
        - Product Group
    parameters:
        - name: id
          in: path
          description: id product group
          required: true
          schema:
            type: string
    responses:
        200:
          description: A product group.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiResponse'
    """
    try:
        value = int(id)
    except:
        raise RequestError("id must be number")
    productGroup = ProductGroupController()    
    result = productGroup.productGroupService.get_by_id(value)         
    return productGroup.response_data(result)


