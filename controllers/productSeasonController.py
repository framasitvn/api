from flask import Blueprint, jsonify, Response, request
from app_response import RequestError
from flask_cors import cross_origin
from controllers.baseController import BaseController
from services.productSeasonService import ProductSeasonService
from requestEntity.productSeasonRequest import ProductSeasonRequest
from requestEntity.productSeasonPriceRequest import ProductSeasonPriceRequest
from requestEntity.productSeasonImageRequest import ProductSeasonImageRequest
from requestEntity.productSeasonComponentRequest import ProductSeasonComponentRequest
from requestEntity.productSeasonShoeFactoryRequest import ProductSeasonShoeFactoryRequest
from requestEntity.baseRequest import ApiRequest

productSeasonController = Blueprint('productSeasonController', __name__)

class ProductSeasonController(BaseController):
    
    def __init__(self):
        super().__init__()
        self.ProductSeasonService = ProductSeasonService()


@productSeasonController.route('/api/productseason/<id>', methods=['POST'])
def get_product_season_by_id(id:int):
    """
    Get product season by id
    ---
    description: >
        Get product season by id
        getting it using the **id** parameter provided!
    tags:
        - Product Season
    parameters:
        - name: id
          in: path
          description: id product season
          required: true
          schema:
            type: string
    responses:
        200:
          description: A product season.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiResponse'
    """
    try:
        value = int(id)
    except:
        raise RequestError("id must be number")
    product_season = ProductSeasonController()
    result = product_season.ProductSeasonService.get_product_season_by_id(value)
    return product_season.response_data(result, 200)    
  
@productSeasonController.route('/api/productseason/by_factory/<factory_id>', methods=['POST'])
def get_product_season_by_factory_id(factory_id:int):
    """
    Get product season list by factory id
    ---
    description: >
        Get product season list by factory id
        getting it using the **factory_id** parameter provided!
    tags:
        - Product Season
    parameters:
        - name: factory_id
          in: path
          description: factory id
          required: true
          schema:
            type: string
    responses:
        200:
          description: product season list.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiResponse'
    """
    try:
        value = int(factory_id)
    except:
        raise RequestError("factory_id must be number")
    product_season = ProductSeasonController()
    result = product_season.ProductSeasonService.get_product_season_by_factory_id(value)
    return product_season.response_data(result, 200)   

@productSeasonController.route('/api/price/by_product_season_id/<product_season_id>', methods=['POST'])
def get_price_by_product_season_id(product_season_id:int):
    """
    Get price list by product season id
    ---
    description: >
        Get price list by product season id
        getting it using the **product_season_id** parameter provided!
    tags:
        - Product Season
    parameters:
        - name: product_season_id
          in: path
          required: true
          schema:
            type: string
    responses:
        200:
          description: price list.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiResponse'
    """
    try:
        value = int(product_season_id)
    except:
        raise RequestError("product_season_id must be number")
    product_season = ProductSeasonController()
    result = product_season.ProductSeasonService.get_prices_by_product_season_id(value)
    return product_season.response_data(result, 200)   

@productSeasonController.route('/api/component/by_product_season_id/<product_season_id>', methods=['POST'])
def get_component_by_product_season_id(product_season_id:int):
    """
    Get component list by product season id
    ---
    description: >
        Get component list by product season id
        getting it using the **product_season_id** parameter provided!
    tags:
        - Product Season
    parameters:
        - name: product_season_id
          in: path
          required: true
          schema:
            type: string
    responses:
        200:
          description: component list.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiResponse'
    """
    try:
        value = int(product_season_id)
    except:
        raise RequestError("product_season_id must be number")
    product_season = ProductSeasonController()
    result = product_season.ProductSeasonService.get_components_by_product_season_id(value)
    return product_season.response_data(result, 200)   

@productSeasonController.route('/api/shoe_factory/by_product_season_id/<product_season_id>', methods=['POST'])
def get_shoe_factory_by_product_season_id(product_season_id:int):
    """
    Get shoe factory list by product season id
    ---
    description: >
        Get shoe factory list by product season id
        getting it using the **product_season_id** parameter provided!
    tags:
        - Product Season
    parameters:
        - name: product_season_id
          in: path
          required: true
          schema:
            type: string
    responses:
        200:
          description: shoe factory list.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiResponse'
    """
    try:
        value = int(product_season_id)
    except:
        raise RequestError("product_season_id must be number")
    product_season = ProductSeasonController()
    result = product_season.ProductSeasonService.get_shoes_factory_by_product_season_id(value)
    return product_season.response_data(result, 200)   

@productSeasonController.route('/api/image/by_product_season_id/<product_season_id>', methods=['POST'])
def get_image_by_product_season_id(product_season_id:int):
    """
    Get image list by product season id
    ---
    description: >
        Get image list by product season id
        getting it using the **product_season_id** parameter provided!
    tags:
        - Product Season
    parameters:
        - name: product_season_id
          in: path
          required: true
          schema:
            type: string
    responses:
        200:
          description: image list.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiResponse'
    """
    try:
        value = int(product_season_id)
    except:
        raise RequestError("product_season_id must be number")
    product_season = ProductSeasonController()
    result = product_season.ProductSeasonService.get_images_by_product_season_id(value)
    return product_season.response_data(result, 200)   


# @requires_auth
#@cross_origin(headers=["Content-Type", "Authorization"])
@productSeasonController.route('/api/productseason/add', methods=['POST'])
@cross_origin(headers=["Content-Type", "application/json"])
def add():
    """
    Add product season
    ---
    description: >
        Add product season        
    tags:
        - Product Season
    requestBody:
        description: The product season to create.    
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/ProductSeasonRequest'
    responses:
        200:
          description: product season.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiResponse'        
    """
    meta_request = request.get_json()        
    api = ApiRequest(meta_request)
    ps_request = ProductSeasonRequest(api.Data)
    ps_request.Username = api.Username

    ps_price_request = [ProductSeasonPriceRequest(price, username= api.Username) for price in ps_request.Prices]
    #ps_price_request = ProductSeasonPriceRequest(ps_request.Prices, username= ps_request.Username)
    ps_image_request = ps_component_request = ps_shoe_factory_request = None
    if ps_request.Images is not None:
        ps_image_request = [ProductSeasonImageRequest(image, username= api.Username) for image in ps_request.Images]
    if ps_request.Components is not None:
        ps_component_request = []
        totalShotWeight = 0
        for component in ps_request.Components:
            componentRequest = ProductSeasonComponentRequest(component, username= api.Username)
            ps_component_request.append(componentRequest)
            totalShotWeight += componentRequest.ShotWeight
        ps_request.TotalShotWeight = totalShotWeight    

    if ps_request.ShoeFactories is not None:
        ps_shoe_factory_request = [ProductSeasonShoeFactoryRequest(shoe_factory, username= api.Username) for shoe_factory in ps_request.ShoeFactories]

    ps_controller = ProductSeasonController()        
    result = ps_controller.ProductSeasonService.add(ps_request, ps_price_request, ps_component_request, ps_image_request, ps_shoe_factory_request)
    return ps_controller.response_data(result, 200)
    
@productSeasonController.route('/api/productseason/modify', methods=['POST'])
@cross_origin(headers=["Content-Type", "application/json"])
def update():
    """
    Update product season
    ---
    description: >
        Update product season        
    tags:
        - Product Season
    requestBody:
        description: The product season to update.    
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/UpdateProductSeasonRequest'
    responses:
        200:
          description: product season.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiResponse'        
    """
    meta_request = request.get_json()  
    api = ApiRequest(meta_request)
    ps_request = ProductSeasonRequest(api.Data, id= 0)   
    
    ps_request.Username = api.Username
    ps_price_request = [ProductSeasonPriceRequest(price, id= 0, product_season_id= ps_request.Id, username= api.Username) for price in ps_request.Prices]
    ps_image_request = ps_component_request = ps_shoe_factory_request = None
    
    if ps_request.Images is not None:
        ps_image_request = [ProductSeasonImageRequest(image, id= 0, product_season_id= ps_request.Id, username= api.Username) for image in ps_request.Images]
    if ps_request.Components is not None:
        ps_component_request = [ProductSeasonComponentRequest(component, id= 0, product_season_id= ps_request.Id, username= api.Username) for component in ps_request.Components]
    if ps_request.ShoeFactories is not None:
        ps_shoe_factory_request = [ProductSeasonShoeFactoryRequest(shoe_factory, id= 0, product_season_id= ps_request.Id, username= api.Username) for shoe_factory in ps_request.ShoeFactories]

    ps_controller = ProductSeasonController()        
    result = ps_controller.ProductSeasonService.update(ps_request, ps_price_request, ps_component_request, ps_image_request, ps_shoe_factory_request)
    return ps_controller.response_data(result, 200)
    

@productSeasonController.route('/api/productseason/delete/<id>', methods = ['POST'])
def delete(id: int):
    
    """
    Deleting product season by id
    ---
    description: >
        Delete product season by id
        getting it using the **id** parameter provided!
    tags:
        - Product Season
    parameters:
        - name: id
          in: path
          description: product season id
          required: true
          schema:
            type: string
    responses:
        200:
          description: A product season.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiResponse'
    """
    try:
        #product_season_id = request.from_values('id')    
        product_season_id = int(id)
    except:
        raise RequestError("id must be number")

    productSeason = ProductSeasonController() 
    result = productSeason.ProductSeasonService.delete(product_season_id)
    return productSeason.response_data(result, 200, "Deleted")

    