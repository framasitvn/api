from requestEntity.baseRequest import BaseRequest
from services.categoryService import CategoryService
from flask import Blueprint, jsonify, Response, request
from app_response import RequestError
from flask_cors import cross_origin
from controllers.baseController import BaseController

categoryController = Blueprint('categoryController', __name__)

class CategoryController(BaseController):
    
    def __init__(self):
        super().__init__()
        self.categoryService = CategoryService()

# @requires_auth
#@cross_origin(headers=["Content-Type", "Authorization"])
@categoryController.route('/api/category/all', methods=['POST'])
def get_all():  
    """
    Get all categories
    ---
    tags:
        - Category
    description: Get all categories.
    responses:
        200:
          description: All categories
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiResponse'
    """
    category = CategoryController()
    result = category.categoryService.get_all()         
    return category.response_data(result)
    

@categoryController.route('/api/category/<id>', methods=['POST'])
def get_by_id(id: int):
    """
    Get category by id
    ---
    description: >
        Get category by id
        getting it using the **id** parameter provided!
    tags:
        - Category
    parameters:
        - name: id
          in: path
          description: id category
          required: true
          schema:
            type: string
    responses:
        200:
          description: A category.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiResponse'
    """
    try:
        value = int(id)
    except:
        raise RequestError("id must be number")
    category = CategoryController()    
    result = category.categoryService.get_by_id(value)         
    return category.response_data(result,200)


