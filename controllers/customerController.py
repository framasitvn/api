from services.customerService import CustomerService
from flask import Blueprint, jsonify, Response, request
from app_response import RequestError
from flask_cors import cross_origin
from controllers.baseController import BaseController

customerController = Blueprint('customerController', __name__)

class CustomerController(BaseController):
    
    def __init__(self):
        super().__init__()
        self.customerService = CustomerService()

# @requires_auth
#@cross_origin(headers=["Content-Type", "Authorization"])
@customerController.route('/api/customer/all', methods=['POST'])
def get_all():   
    """
    Get all customers
    ---
    tags:
        - Customer
    description: Get all customers.
    responses:
        200:
          description: All customers
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiResponse'
    """
    customer = CustomerController()    
    result = customer.customerService.get_all()         
    return customer.response_data(result)

@customerController.route('/api/customer/<id>', methods=['POST'])
def get_by_id(id: int):
    """
    Get customer by id
    ---
    description: >
        Get customer by id
        getting it using the **id** parameter provided!
    tags:
        - Customer
    parameters:
        - name: id
          in: path
          description: id customer
          required: true
          schema:
            type: string
    responses:
        200:
          description: A customer.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiResponse'
    """
    try:
        value = int(id)
    except:
        raise RequestError("id must be number")
    customer = CustomerController()    
    result = customer.customerService.get_by_id(value)         
    return customer.response_data(result)


