from services.shoeFactoryService import ShoeFactoryService
from flask import Blueprint, jsonify, Response, request
from app_response import RequestError
from flask_cors import cross_origin
from controllers.baseController import BaseController

shoeFactoryController = Blueprint('shoeFactoryController', __name__)

class ShoeFactoryController(BaseController):
    
    def __init__(self):
        super().__init__()
        self.shoeFactoryService = ShoeFactoryService()

# @requires_auth
#@cross_origin(headers=["Content-Type", "Authorization"])
@shoeFactoryController.route('/api/shoefactory/all', methods=['POST'])
def get_all():   
    """
    Get all shoes factory
    ---
    tags:
        - Shoe Factory
    description: Get all shoes factory.
    responses:
        200:
          description: All shoes factory
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiResponse'
    """
    shoeFactory = ShoeFactoryController()    
    result = shoeFactory.shoeFactoryService.get_all()         
    return shoeFactory.response_data(result)

@shoeFactoryController.route('/api/shoefactory/<id>', methods=['POST'])
def get_by_id(id: int):
    """
    Get shoe factory by id
    ---
    description: >
        Get shoe factory by id
        getting it using the **id** parameter provided!
    tags:
        - Shoe Factory
    parameters:
        - name: id
          in: path
          description: id shoe factory
          required: true
          schema:
            type: string
    responses:
        200:
          description: A shoe factory.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiResponse'
    """
    try:
        value = int(id)
    except:
        raise RequestError("id must be number")
    shoeFactory = ShoeFactoryController()    
    result = shoeFactory.shoeFactoryService.get_by_id(value)         
    return shoeFactory.response_data(result)


