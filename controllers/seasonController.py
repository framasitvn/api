from services.seasonService import SeasonService
from controllers.baseController import BaseController
from requestEntity.seasonRequest import SeasonRequest
from requestEntity.baseRequest import ApiRequest, Action
from app_response import RequestError
from flask import Blueprint, jsonify, Response, request
from flask_cors import cross_origin


seasonController = Blueprint('seasonController', __name__)

class SeasonController(BaseController):
    
    def __init__(self):
        super().__init__()                
        self.seasonService = SeasonService()
        
    def get(self):
        return self.seasonService.get_all()
    

# @requires_auth
#@cross_origin(headers=["Content-Type", "Authorization"])
@seasonController.route('/api/season/all', methods=['POST'])
def get_all():    
    """
    Get all seasons
    ---
    tags:
        - Season
    description: Get all seasons.
    responses:
        200:
          description: All seasons
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiResponse'
    """
    season = SeasonController()    
    result = season.seasonService.get_all()         
    return season.response_data(result)
    

@seasonController.route('/api/season/<id>', methods=['POST'])
def get_by_id(id:int):
    """
    Get season by id
    ---
    description: >
        Get season by id
        getting it using the **id** parameter provided!
    tags:
        - Season
    parameters:
        - name: id
          in: path
          description: id season
          required: true
          schema:
            type: string
    responses:
        200:
          description: A season.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiResponse'
    """
    try:
        value = int(id)
    except:
        raise RequestError("id must be number")
    season = SeasonController()
    result = season.seasonService.get_by_id(value)            
    return season.response_data(result, 200)
    

@seasonController.route('/api/season/add', methods = ['POST'])
@cross_origin(headers=["Content-Type", "application/json"])
def add():
    """
    Add season
    ---
    description: >
        Add season        
    tags:
        - Season
    requestBody:
        description: The season to create.    
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/SeasonRequest'
    responses:
        200:
          description: season.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiResponse'        
    """
    meta_request = request.get_json()        
    api = ApiRequest(meta_request)
    ss_request = SeasonRequest(api.Data)
    ss_request.Username = api.Username
    season = SeasonController()
    result = season.seasonService.add_season(ss_request)
    return season.response_data(result)
    
@seasonController.route('/api/season/modify', methods = ['POST'])
@cross_origin(headers=["Content-Type", "application/json"])
def update():
    """
    Update season
    ---
    description: >
        Update season        
    tags:
        - Season
    requestBody:
        description: The season to update.    
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/UpdateSeasonRequest'
    responses:
        200:
          description: season.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiResponse'        
    """
    meta_request = request.get_json()  
    api = ApiRequest(meta_request)
    ss_request = SeasonRequest(api.Data, id= 0)
    ss_request.Username = api.Username
    
    comp = SeasonController()
    result = comp.seasonService.update_season(ss_request)
    return comp.response_data(result, 200)
    

@seasonController.route('/api/season/inactivated', methods = ['POST'])
@cross_origin(headers=["Content-Type", "application/json"])
def inactivated():
    """
    Deactivated season
    ---
    description: >
        Deactivated season        
    tags:
        - Season
    requestBody:
        description: make season to inactive.    
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/InactiveMasterRequest'
    responses:
        200:
          description: season.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiResponse'        
    """
    meta_request = request.get_json()        
    api = ApiRequest(meta_request)
    ss_request = SeasonRequest(api.Data, id= 0, action= Action.DEACTIVE)
    ss_request.Username = api.Username
    season = SeasonController()
    result = season.seasonService.delete_season(ss_request)
    return season.response_data(result)


