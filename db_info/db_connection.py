import pymssql
from db_info.db_config import config


class Connection():

    __Server = config['Server']
    __UserID = config['Username']
    __Password = config['Password']
    InitCatalog = config['Database']
    DataWarehouse = config['DataWarehouse']

    def __init__(self, connect=None, database = InitCatalog):

        def new_connection():
            """
            Return connection string can connect to SQL Server
            """
            connect = pymssql.connect(server = self.__Server, 
                                    user = self.__UserID, 
                                    password = self.__Password, 
                                    database = database)            
            return connect
            
        self.Connect = connect if connect is not None else new_connection()
        self.Cursor = self.Connect.cursor()


    def get_data(self, strQuery):
        try:
            self.Cursor.execute(strQuery)            
            results = self.__binding_data(self.Cursor)
        except Exception as ex:
            results = { "Data": None, "Exception": ex }
        # finally:
        #     self.Cursor.close()  
        return results

    def get_autocommit_mode(self):
        return self.Connect.autocommit_state

    def set_autocommit_mode(self, mode):
        self.Connect.autocommit(mode)

    def begin_transaction(self):
        """
        Begin transaction
        """
        # cursor = self.Connect.cursor()
        self.Cursor.execute("BEGIN TRANSACTION")
        

    def commit(self):
        self.Connect.commit()
        self.close()

    def rollback(self):
        '''
        Rollback transaction
        '''
        self.Connect.rollback()
        self.close()

    def close(self):
        """
        Close connection
        """
        if self.Cursor:
            self.Cursor.close()
        if self.Connect:
            self.Connect.close()
        
    def execute_query(self, strQuery):
        try:
            # cursor = self.Connect.cursor()
            results = self.Cursor.execute(strQuery)
        except Exception as ex:
            results = { "Data":None, "Exception": ex }
        return results
    
    def get_data_by_stored_procedure(self, stored_name, parameters):
        try:     
            # cursor = self.Connect.cursor()               
            self.Cursor.callproc(stored_name, parameters)
            self.Cursor.nextset()      
            results = self.__binding_data(self.Cursor)
        except Exception as ex:
            results = { "Data":None, "Exception": ex }
        # finally:
        #     self.Cursor.close()
        return results

    def execute_stored_procedure(self, stored_name, parameters):
        try:          
            # cursor = self.Connect.cursor()
            results = self.Cursor.callproc(stored_name, parameters)
        except Exception as ex:
            results = { "Data":None, "Exception": ex }  
        return results
       
    def create_output_parameter(self, data_type= None):
        return pymssql.output(type(data_type), data_type)
        
    def __binding_data(self, cursor):
        if cursor.description is not None:                                    
            cols = [d[0] for d in cursor.description]
            results = [dict(zip(cols, row)) for row in cursor.fetchall()]    
        else:
            results = None
        return results
