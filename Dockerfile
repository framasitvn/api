# Dockerfile - this is a comment. Delete me if you want.
FROM python:3.7.4

COPY . /app
WORKDIR /app
EXPOSE 6000
RUN apt-get install gcc
ENV TZ=Asia/Ho_Chi_Minh
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN pip install --upgrade pip
#RUN pip install flask flask_restful pyodbc
RUN pip install -r requirements.txt

ENTRYPOINT ["python"]
CMD ["app.py"]
