from flask import Flask, jsonify, request, Response, session, redirect, url_for, render_template
#from flask_restful import Api, Resource
from flask_session import Session
from flask_cors import CORS, cross_origin
from app_response import AuthError, RequestError, ResponseData
from flasgger import Swagger, swag_from
import app_config
#from repository import db_config
import msal
import uuid

#report 
from controllers.reportController import reportController 
#master from data warehouse
from controllers.factoryController import factoryController 
from controllers.componentController import componentController 
from controllers.productSeasonController import productSeasonController 
from controllers.seasonController import seasonController , SeasonController
from controllers.contactInfoController import contactInfoController 
#data from own database
from controllers.categoryController import categoryController 
from controllers.customerController import customerController 
from controllers.productGroupController import productGroupController 
from controllers.projectController import projectController 
from controllers.shoeFactoryController import shoeFactoryController 
from controllers.toolingNoController import toolingNoController 
from controllers.countryController import countryController 


app = Flask(__name__)
swagger = Swagger(app, template_file='swagger_template.yml')
app.register_blueprint(reportController)

app.register_blueprint(factoryController)
app.register_blueprint(componentController)
app.register_blueprint(productSeasonController)
app.register_blueprint(seasonController)
app.register_blueprint(contactInfoController)

app.register_blueprint(categoryController)
app.register_blueprint(customerController)
app.register_blueprint(productGroupController)
app.register_blueprint(projectController)
app.register_blueprint(shoeFactoryController)
app.register_blueprint(toolingNoController)
app.register_blueprint(countryController)

app.config.from_object(app_config)

# api = Api(app)
# api.add_resource(SeasonController, '/')

#app.config.from_object(db_config)
app.config['CORS_ALLOW_HEADERS'] = "Content-Type"
app.config['CORS_RESOURCES'] = {r"/api/*": {"origins": "*"}}

cors = CORS(app)

@app.errorhandler(AuthError)
def handle_auth_error(ex):
    response = jsonify(ex.error)
    response.status_code = ex.status_code
    return response

@app.errorhandler(RequestError)
def handle_request_error(ex):
    response = jsonify(ex.error)
    response.status_code = ex.status_code
    
    return response

@app.route("/")
def index():
    # if not session.get("user"):
    #     return redirect(url_for("login"))
    # return render_template('index.html', user=session["user"], version = msal.__version__)
    return redirect("/apidocs")

@app.route("/login")
def login():
    """
    Login to Customer Category system
    ---
    tags:
        - System
    description: Login to Customer Category system
    response:
        redirect to login page
    """
    session["state"] = str(uuid.uuid4())
    auth_url = _build_auth_url(scopes= app_config.SCOPE, state= session["state"])
    return render_template("login.html", auth_url= auth_url, version= msal.__version__ )

@app.route("/logout")
def logout():
    """
    Logout system
    ---
    description: >
        Wipe out user and its token cache from session
        Also logout from your tenant's web session
    tags:
        - System
    response:
        200:
        description: logout successful
    """
    session.clear() 
    return redirect(app_config.AUTHORITY + "/oauth2/v2.0/logout?post_logout_redirect_uri=" + url_for("index", _external=True))

#@app.route(app_config.REDIECT_PATH, methods=['POST'])
@app.route('/token', methods=['POST'])
def demo_authorized():
    username = request.form.get('username')
    if not username:
        return ResponseData(100, "username is required").response()
    cache = _load_cache()
    cca = _build_msal_app(cache= cache)
    print ('cca: ', cca.__dict__)
    print('token_cache:', cca.token_cache.__dict__)
    account = cca.get_accounts()
    print ('account: ',account)
    #result = cca.acquire_token_by_authorization_code(code = code, scopes = app_config.SCOPE)
    result = cca.acquire_token_silent(app_config.SCOPE, account = account )
    print("result: ", result)
    # if "error" in result:
    #     res = ResponseData(401, result)
    #     return res.response()
    # session["user"] = result.get("id_token_claims")
    _save_cache(cache)
    

@app.route(app_config.REDIECT_PATH)
def authorized():
    if request.args.get('state') != session["state"]:
        return redirect(url_for("index"))
    if "error" in request.args:
        return render_template("auth_error.html", result= request.args)
    if request.args.get('code'):
        cache = _load_cache()
        result = _build_msal_app(cache= cache).acquire_token_by_authorization_code(
                                                    request.args["code"], 
                                                    scopes= app_config.SCOPE, 
                                                    redirect_uri=url_for("authorized", _external = True))
        if "error" in result:
            return render_template("auth_error.html", result= result)
        session["user"] = result.get("id_token_claims")
        _save_cache(cache)

    return redirect(url_for("index"))


def _load_cache():
    cache = msal.SerializableTokenCache()
    if session.get("token_cache"):
        cache.deserialize(session["token_cache"])
    return cache

def _save_cache(cache):
    if cache.has_state_changed:
        session["token_cache"] = cache.serialize()

def _build_msal_app(cache=None, authority=None):
    return msal.ConfidentialClientApplication(
        app_config.CLIENT_ID, authority=authority or app_config.AUTHORITY,
        client_credential=app_config.CLIENT_SECRET, token_cache=cache)

def _build_auth_url(authority=None, scopes=None, state=None):
    return _build_msal_app(authority=authority).get_authorization_request_url(
        scopes or [],
        state=state or str(uuid.uuid4()),
        redirect_uri=url_for("authorized", _external=True))

def _get_token_from_cache(scope=None):
    cache = _load_cache()  # This web app maintains one cache per session
    cca = _build_msal_app(cache=cache)
    accounts = cca.get_accounts()
    if accounts:  # So all account(s) belong to the current signed-in user
        result = cca.acquire_token_silent(scope, account=accounts[0])
        _save_cache(cache)
        return result


if __name__ == "__main__":
    # app.run(debug=True, host="0.0.0.0", port=6000)
    # app.run(debug=True)
    app.run(port=6001)
    